const redis = require('../data/redis');
class Subscriber {
  constructor() {
    this.subscriber = redis.client.duplicate()
    console.log('Subscriber created')
  }

  async connect() {
    try {
      // await this.subscriber.connect();
      console.log('subscriber connected  succesfully ');
    } catch (err) {
      throw err;
    }
  }

  async subscribe(channel, fn) {
    try {
      await this.subscriber.subscribe(channel, fn)
    } catch (err) {

    }
  }

}
module.exports = new Subscriber()


// async (msg) => {
//   const message = JSON.parse(msg)
//   if (BAGS_MESSAGES_TYPES.includes(message.event_type)) {
//     await hset(`controllers`, message.mac, JSON.stringify({ from: message.from, connected_on: message.connected_on, last_message: message.timestamp, event_type: message.event_type }))
//   }
// }