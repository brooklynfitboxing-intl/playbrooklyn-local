const axios = require('axios');
/**
 * @type {import('axios').AxiosInstance}
 */
const fitzone = axios.create({
    baseURL: process.env.fitzone_url,
    timeout: 10*1000 
});
/**
 * @type {import('axios').AxiosInstance}
 */
const authServer = axios.create({
    baseURL: process.env.auth_server,
    timeout: 10*1000 
});
/**
 * @type {import('axios').AxiosInstance}
 */
const fwgServer = axios.create({
    baseURL: process.env.fwg_server,
    timeout: 10*1000 
});
// Override timeout default for the library
// Now all requests using this instance will wait 2.5 seconds before timing out
fitzone.defaults.timeout = 5500;

// Override timeout for this request as it's known to take a long time
module.exports = { fitzone, fwgServer, authServer }