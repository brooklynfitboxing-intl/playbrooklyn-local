const { createClient } = require('redis');
const REDIS_CONFIG = {
    host: process.env.redis_host || 'localhost',
    port: process.env.redis_port || 6379
}
const redisClient = createClient(REDIS_CONFIG);

redisClient.on('connect', function (cc) {
    console.log(`Redis client connected succesfully `, REDIS_CONFIG );
})  

module.exports = {
    get: redisClient.get.bind(redisClient),
    set: redisClient.set.bind(redisClient),
    getList: redisClient.lRange.bind(redisClient),
    hgetall: redisClient.hGetAll.bind(redisClient),
    hget: redisClient.hGet.bind(redisClient),
    hset: redisClient.hSet.bind(redisClient),
    mget: redisClient.mGet.bind(redisClient),
    mset: redisClient.mSet.bind(redisClient),
    publish: redisClient.publish.bind(redisClient),
    client: redisClient,
}