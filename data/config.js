const mysql = require('mysql');
const util = require('util');
const { networkInterfaces } = require('os');
const config = {
    host: process.env.db_host,
    user: process.env.db_user,
    password: process.env.db_password,
    database: process.env.db_name,
};

const conn = mysql.createPool(config);

conn.query('SELECT 1 + 1 AS dummtest', (error, results, fields) => {
    if (error) {
        console.error("mysql connection error", error);
        throw error
    }
    console.log("mysql connection succesfully", config.host, config.user, config.database);
});

module.exports = conn;