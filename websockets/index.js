const winston = require('winston');
const io = require("socket.io")

const logger = winston.createLogger({
    level: 'info',
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'logs/player.log' })
    ]
});
const DailyRotateFile = require('winston-daily-rotate-file');
logger.configure({
    level: 'verbose',
    transports: [
        new DailyRotateFile({
            filename: 'logs/player-%DATE%.log',
            datePattern: 'YYYY-MM-DD-HH',
            zippedArchive: true,
            maxSize: '10m',
            maxFiles: '5d'
        })
    ]
})
/**
 * 
 * @param {Server} server 
 */
async function init(server) {
    console.log('Starting websocket')
    const websSocketPlayer = io(server)
    websSocketPlayer.on('connection', (socket) => {
        console.log(`player connected`)
        socket.on('event', (data) => {
        });
        socket.on('log', (data) => {
            logger.log('info', data)
        });
        // when the user disconnects.. perform this
        socket.on('disconnect', (data) => {
        })
    });

    // const remoteControlNamespace = websSocketPlayer.of('/remote');

    // remoteControlNamespace.use((socket, next) => {
    //     // ensure the user has sufficient rights
    //     next();
    // });

    // remoteControlNamespace.on('connection', socket => {
    //     // console.log('Remote control attached to this center')
    //     //TODO posibles acciones, Reproducir clase, ver lista de videos del challenge, reproducir videos 
    //     // remoteControlNamespace.emit('remoteAttached', 'remote control attached')
    //     // socket.on('startClass', () => {
    //     //   // ... 
    //     //   remoteControlNamespace.emit('startClass', {
    //     //     data: {

    //     //     }
    //     //   })
    //     // });

    //     // socket.on('playVideo', () => {
    //     //   remoteControlNamespace.emit('playVideo', {
    //     //     data: {

    //     //     }
    //     //   })
    //     // });

    // });
}
module.exports = { init };