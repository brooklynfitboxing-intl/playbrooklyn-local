module.exports = {
  apps : [{
    name:'playbrooklyn-server',
    script: './bin/www',
    exec_mode:"fork",
           
    watch: true,
    //Deberia arrancarlo el usuario minipc para quelos logs  esten en /home/minipc/.pm2/logs
    time: true,
    autorestart:true,
    env: {
      "NODE_ENV": "development",
      "playserver_path": "/var/www/playserver",
      "db_name": "brooklyn",
      "db_user": "root",
      "db_password": "camaleon!",
      "db_host": "localhost",
      "fitzone_url": "https://myh4c.brooklynfitzone.com/iot/iot",
      "auth_server": "http://10.10.0.4:3000",
      "fwg_server": "10.10.0.4:3030",
    },
    env_production : {
       "NODE_ENV": "production",
       "playserver_path": "/var/www/playserver",
       "db_name": "brooklyn",
       "db_user": "root",
       "db_password": "camaleon!",
       "db_host": "localhost",
       "fitzone_url": "https://myh4c.brooklynfitzone.com/iot/iot",
       "auth_server": "https://brooklynfitzone.com/auth",
       "fwg_server": "https://brooklynfitzone.com/fwg"
    },
    env_production_f3 : {
      "NODE_ENV": "production",
      "playserver_path": "/var/www/playserver",
      "db_name": "brooklyn",
      "db_user": "root",
      "db_password": "camaleon!",
      "db_host": "localhost",
      "fitzone_url": "https://app.f3fitbox.com/iot/iot/",
   }
  
  
  }],
  
};
