let classDataErr = {
  "id" : 'err',
  'desc' :'Errorcito jeje'
}

let classData = 
{
  "id": 498768,
  "users": [
    {
      "id": 5585,
      "sb": 3,
      "nick": "María",
      "gender": "F",
      "photo": "woman_ico.png",
      "logo": "",
      "range": "B",
      "level": 3
    },
    {
      "id": 19725,
      "sb": 2,
      "nick": "Ayito",
      "gender": "F",
      "workout": 85.1,
      "photo": "profiles/19725_1491920001.jpg",
      "range": "S",
      "level": 9
    },
    {
      "id": 19997,
      "sb": 4,
      "nick": "Lorena",
      "gender": "F",
      "workout": 82.2,
      "photo": "profiles/19997_1497990393.jpg",
      "range": "G",
      "level": 3
    },
    {
      "id": 20934,
      "sb": 5,
      "nick": "Jasar",
      "gender": "M",
      "workout": 92.8,
      "photo": "profiles/e481928db58282df80aac822cc4a1de0.jpg",
      "range": "B",
      "level": 0
      
    },
    {
      "id": 48978,
      "sb": 1,
      "nick": "Saray",
      "gender": "M",
      "workout": 0,
      "power": 0,
      "photo": "man_ico.png",
      "range": "B",
      "level": 8
    }
  ],
  minigames: true,
  teamA: [
    19725,
    5585
  ],
  teamB: [
    48978,
    20934,
    19997
  ]
  }
/*   {
    "id": 675463,
    "minigame": true,
    "teams": "M vs F",
    "users": [
      {
      "sb": 1,
      "nick": "Tyrus",
      "gender": "M",
      "workout": 90.3,
      "power": 238,
      "photo": "profiles/ba0290f5c193b4faaff947eb37f78a42.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1,
      "team": 'B'
    },
    {
      "sb": 3,
      "nick": "Dale",
      "gender": "F",
      "workout": 88.6,
      "power": 312,
      "photo": "profiles/c35a81855498d853422932d2c16e46fc.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1,
      "team": 'B'
    },
    {
      "sb": 5,
      "nick": "Archie",
      "gender": "M",
      "workout": 90.4,
      "power": 245,
      "photo": "profiles/a391371190d2e4e58331a8784bad2b37.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1,
      "team": 'B'
    },
    {
      "sb": 7,
      "nick": "Carolan",
      "gender": "F",
      "workout": 91.5,
      "power": 262,
      "photo": "profiles/464e7d81613253fe6feafaa4af885c7f.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1
    },
    {
      "sb": 10,
      "nick": "Karlen",
      "gender": "F",
      "workout": 89.4,
      "power": 154,
      "photo": "profiles/c34ed33f8928f66da4ae0faf4bd7d8a9.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1
    },
    {
      "sb": 22,
      "nick": "Fraser",
      "gender": "F",
      "workout": 91.8,
      "power": 377,
      "photo": "profiles/e72cb186e0b119ca132b203c2b7c22ae.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1,
      "team": 'Y'
    },
    {
      "sb": 13,
      "nick": "Maria",
      "gender": "M",
      "workout": 90.4,
      "power": 172,
      "photo": "profiles/781190a9504af426f2aa2244d9533451.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1,
      "team": 'Y'
    },
    {
      "sb": 15,
      "nick": "Jarad",
      "gender": "M",
      "workout": 89.9,
      "power": 234,
      "photo": "profiles/dd97bdcb06383389c9a94cb4cd6f18c1.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1,
      "team": 'Y'
    },
    {
      "sb": 17,
      "nick": "Ado",
      "gender": "M",
      "workout": 89.2,
      "power": 128,
      "photo": "profiles/95a4586768bfd78a2caa2aa9f3300bbb.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1,
      "team": 'B'
    },
    {
      "sb": 20,
      "nick": "Ronny",
      "gender": "M",
      "workout": 89.3,
      "power": 147,
      "photo": "profiles/11441410a4d7b29a0b6d331fba677a15.png",
      "logo": "",
      "fbp_mult": 1,
      "pwp_mult": 1,
      "team": 'B'
    }
    ]
} */


var express = require('express');
var router = express.Router();

// router.get('/', function(req, res, next) {
//    res.send(challenges);
// });
router.get('/:id', (req, res) => {
  const id = String(req.params.id);
 
  res.send(classData);
  //res.send(classDataErr);

});

module.exports = router;

