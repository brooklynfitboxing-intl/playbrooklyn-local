var express = require('express');
var router = express.Router();
var fs = require('fs');
const path = require('path');

router.get('/', function(req, res, next) {
  var fitboxerFile =   path.join(__dirname +'/../'+'/public/resources/fitboxers.json');
  let rawdata = fs.readFileSync(fitboxerFile);
  let fitboxer = JSON.parse(rawdata);
  res.send(fitboxer);
});


module.exports = router;
