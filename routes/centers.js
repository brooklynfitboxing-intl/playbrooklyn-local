var express = require('express');
var router = express.Router();
var fs = require('fs');
const path = require('path');
const mysql = require('../data/config');
const axios = require('axios');
const fetch = require('node-fetch');
const { getCenter, setCenter } = require('../services/center');
const { fitzone, authServer } = require('../data/axios');

async function getVPNKey({ centerName, env }) {
	const url = `http://vpn.yustplayit.com/pb/get_client_key.php?client=${centerName}&center_env=${env}`;
	try {
		const response = await fetch(url);
		const responseText = await response.text();
		let filePath = `${env}.txt`
		let fileContent = responseText
		fs.writeFile(filePath, fileContent, (err) => {
			if (err) throw err;
			console.log('file saved')
		})
	} catch (e) {
		console.error(e)
		throw (e);
	}
}

router.get('/', async function (req, res, next) {
	res.send({ status: true, data: await getCenter() });
});

router.post('/', async function (req, res, next) {
	const { center } = req.body;
	setCenter(center);
	res.send({ status: true, data: await getCenter() });
});

// router.post('/verify', async function (req, res, next) {
// 	try {
// 		const verify = await authServer.get(`/user/verify`, {
// 			headers: req.headers.authorization
// 		})
// 		res.send(verify.data)
// 	} catch (err) {
// 		res.status(403).send({ status: false })
// 	}
// })

router.post('/login', async function (req, res, next) {
	const email = req.body.email
	const password = req.body.password
	try {
		const { data } = await authServer.post(`/user/login`, { email, password })
		console.log(data.permissions)
		let districtAssigned = null;
		if (data.permissions) {
			districtAssigned = data.permissions.filter(it => it.role === 'MINIPC')[0]
		}
		if (!districtAssigned) {
			res.status(403).json({
				message: 'No center assigned for this user',
				status: false,
			})
			return
		}
		fitzone.defaults.headers.Authorization = `Bearer ${data.token}`
		res.json({
			center: `${districtAssigned.district.alias}/F`,
			token: data.token,
		})
	} catch (err) {
		console.error(err)
		res.status(401).json({
			message: '',
			status: false,
		})
	}
	/* try {
		console.log('try')
		let response = await axios.post("https://pro.play.brooklynfitboxing.com/api/centers/center_login", {
			email, password })
			data = response.data.center
			let center = await getCenter()
			if (center.length > 0) {
				mysql.query('UPDATE centers set name = ?, country = ?, address =?, phone=?, fitzone_alias =?, projector_delay=?, git_branch =?, email_center =?, numberOfBags =?, vpn_address=? where id =2', 
				[data.name, data.country, data.address, data.phone, data.fitzone_alias, data.projector_delay,data.git_branch, data.email_center, data.numberOfBags, data.vpn_address] , async (error, result) => {
					if (error) throw error;
					res.send({status:true,message:"Center updated",fitzone_alias: data.fitzone_alias});
				});
			} else {
				mysql.query('INSERT into centers(id, name, country, city, address, phone, fitzone_alias, projector_delay, git_branch, email_center, numberOfBags, vpn_address, ip, remote_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
				[2, data.name, data.country, data.city, data.address, data.phone, data.fitzone_alias, data.projector_delay, data.git_branch, data.email_center, data.numberOfBags, data.vpn_address, data.ip, data.remote_id], (err, result) => {
					if (err) throw err;
					res.send({status:true,message:"Center created",fitzone_alias:data.fitzone_alias});
				})
			}
			// TODO: conexión a la VPN
			const resp = await getVPNKey({centerName: 'gleyder', env: 'brooklyn'})
			console.log(resp)
			console.log(response.data.center)
	} catch(err){
		console.log(err.response.data.message)
		res.status(401).send( {status:false, message:err.response.data.message});
	} */

});

router.get('/dummy', function (req, res, next) {
	var centerFile = path.join(__dirname + '/../' + '/public/resources/center.json');
	let rawdata = fs.readFileSync(centerFile);
	let center = JSON.parse(rawdata);
	res.send(center);
});

router.get('/:center_id', function (req, res, next) {
	let center_id = req.params.center_id;
	let sql = "SELECT * from centers  where id = ? ";

	mysql.query(sql, [center_id], function (error, result) {
		if (result.length) {
			res.send({ status: true, data: result[0] });
		} else {
			res.send({ status: false, message: 'No center found for id : ' + center_id });
		}
	});
});

module.exports = router