var express = require('express');
var router = express.Router();
var fs = require('fs');
const path = require('path');

router.get('/', function(req, res, next) {
  var bagsFile =   path.join(__dirname +'/../'+'/public/resources/bags.json');
  let rawdata = fs.readFileSync(bagsFile);
  let bag = JSON.parse(rawdata);
  res.send(bag);
});


module.exports = router;