const util = require('util');
const axios = require('axios');
const express = require('express');
var router = express.Router();
const mysql = require('../data/config');
const query = util.promisify(mysql.query).bind(mysql);
const { createMoves, getBags, sendStartVideo, getHitsDetail, sendHitsDetail } = require('../services/bags')
const { exec } = require("child_process");
const {
  set,
  hset,
  get,
  hgetall,
  hget
} = require('../data/redis')
const playserverPath = process.env.playserver_path;
const initializeBagsPoints = async function (numberOfBags) {
  for (let k = 1; k <= numberOfBags; k++) {
    await hset('workout:bags', { [k]: '0:0:0:0:0:0' })
  }
}
router.get('/real_time_results', async (req, res, next) => {
  let fitboxers = await hget('fitboxers', 'data')
  const roundNumber = await get('round_number')
  var bagsList = await hgetall('workout:bags')
  const fitzoneId = await get('fitzone_id')
  if (bagsList == null) {
    bagsList = await query(` SELECT * from bag_rounds  where round_id in (select id from rounds where round_number = ? and class_id in (
                    select id from classes where fitzone_id = ? 
                   ) 
              ) `, [roundNumber, fitzoneId]);
  }

  if (fitboxers === null) {
    //Retorna la puntuacion para todos los  sacos
    fitboxers = [];
    // for(let i=1; i<=numberOfBags;i++){
    bagsList.forEach((bag) => {
      let hits = bag.user_hits
      let fbp = bag.user_fbp
      let pwp = bag.user_pwp
      let totp = bag.user_totp
      let workout = bag.user_workout
      let power = bag.user_power
      fitboxers.push({
        sb: i,
        hits: hits,
        fbp: fbp,
        pwp: pwp,
        totp: totp,
        workout: workout,
        power: power
      });

    })
    // }
  } else {
    fitboxers = JSON.parse(fitboxers);
    // retorna puntuacion para los sacos y fitboxers en el round actual

    fitboxers.forEach(function (fitboxer) {
      // TODO machear con los workouts

      var redisBagPoints = bagsList[fitboxer.sb];
      if (redisBagPoints) {
        redisBagPoints = redisBagPoints.split(":");
        let hits = parseInt(redisBagPoints[0]);
        let fbp = parseInt(redisBagPoints[1]);
        let pwp = parseInt(redisBagPoints[2]);
        let totp = parseInt(redisBagPoints[3]);
        let workout = parseFloat(redisBagPoints[4]);
        let power = parseFloat(redisBagPoints[5]);

        workout = workout > 0 ? workout.toFixed(3) : 0;
        power = power > 0 ? power.toFixed(1) : 0;
        fitboxer.workout = workout;
        fitboxer.power = power;
        fitboxer.pwp = pwp;
        fitboxer.hits = hits;
        fitboxer.fbp = fbp;
        fitboxer.totp = totp;
      }
    });
  }
  res.send({ "message": "", "data": fitboxers });
})

const resetWeAreInClassFile = () => {
  try {
    const roundFlagCommand = "python3 " + playserverPath + "/scripts/reset_we_are_in_class_time_flag.py TRUE";
    console.log("we are in class time flag : " + roundFlagCommand);
    exec(roundFlagCommand);
  } catch (err) {
    console.error(err);
    return;
  }
}

router.post('/:round_number/start_lights', async function (req, res, next) {
  const { fitzone_id, time: start_time = Date.now(), video } = req.body;
  const { round_number } = req.params;
  const projector_delay = 150;
  console.log(`start_lights, fitzone_id:${fitzone_id}, time:${start_time}, round_number:${round_number}, videodata: `, video)
  // const fitzoneId = await get('fitzone_id')
  resetWeAreInClassFile()
  const bpm = video.type.bpm ? video.type.bpm : 0;
  const start_in = video.type.start_in ? video.type.start_in : 17240;
  const sequence = video.type.sequence ? video.type.sequence : "X0X0X0X0";
  const number_of_hits = video.type.hits ? video.type.hits : 0;
  // TODO se remueve temporalmente, proxima release, calcular los golpes en el minipc teniendo en cuenta los delay para cada saco
  // const moves = createMoves({ start_in, sequence, bpm, number_of_hits });
  const video_metadata = {
    type_of_video: 'Round',
    bpm,
    start_in,
    sequence,
    start_clock: start_time,
    round_id: round_number,
    projector_delay,
    hand: "R",
    duration: 120,
    tolerance: 0.15,
    round_id: round_number,
    number_of_hits,

    // users_power: Array(24).fill(null).reduce((acc, _, idx) =>  ( {[idx+1]: 900, ...acc} ), {})
  }
  await hset('round_data', round_number, JSON.stringify(video.type))
  const bags = await getBags();
 
  let fitboxers = await hgetall('fitboxers')
  fitboxers = JSON.parse(fitboxers.data);
  const bagFitboxer = {}
  for (const fb of fitboxers) { // object map 
    if (!bagFitboxer[fb.sb]) {
      bagFitboxer[fb.sb] = fb
    }
  }
  try {
    bags.forEach(async (bag) => { 
      let power = 900;
      if (bagFitboxer[bag.bag_number]) {
        // video_metadata.users_power[bag.bag_number] = bagFitboxer[bag.bag_number].power
        power = bagFitboxer[bag.bag_number].power
      }
      await set(`video_metadata:${bag.bag_number}`, JSON.stringify( { ...video_metadata, power }))
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ status: false, message: err.message });
    return;
  }
  const startVideoMessage = {
    type_of_video: 'Round',
    bpm,
    start_in,
    sequence,
    start_clock: start_time,
    round_id: round_number,
    projector_delay,
    number_of_hits,
    user_power: 900
  }
  const bagsPromises  = bags.map((bag) => {
    if (bagFitboxer[bag.bag_number]) {
      startVideoMessage['user_power'] = bagFitboxer[bag.bag_number] ? bagFitboxer[bag.bag_number].power : 900
    }
    return sendStartVideo(bag, startVideoMessage)
  })
  res.send({ status: true, message: "start lights executed succesfully" });

  const responses = await Promise.allSettled(bagsPromises);
  const rejected = responses.filter(result => result.status === 'rejected');
  if (rejected.length) {
    console.error(`Error sending start video to bags: `, rejected)
  }
});


router.get('/:fitzone_id', async (req, res, next) => {
  let fitzone_id = req.params.fitzone_id;
  var result = await query('SELECT * from rounds where class_id in (SELECT id from classes where fitzone_id = ?) ', [fitzone_id]);
  res.send({ status: true, message: "", data: result });
});

async function setChallengeToClass(challengeId) {
  const fitzoneId = await get('fitzone_id');
  try {
    await query('UPDATE classes set challenge = ? where fitzone_id = ? ', [fitzoneId, challengeId])
  } catch (err) {
    console.log(`Error setting challenge ${challengeId} to class: ${fitzoneId}`)
  }
  return
}

router.post('/:round_number/start_round', async (req, res, next) => {
  const { round_number } = req.params;
  const { challenge_id } = req.body;
  let { users } = req.body 
  const defaultNumberOfBags = 24
  let video_id = 1;
  let start_time_local = new Date();
  const fitzoneId =  await get('fitzone_id')
  console.log(`Start round: ${round_number}, sessionId:[${fitzoneId == '1' ? 'Dummy' : fitzoneId}]`)

  if (round_number == undefined) {
    res.status(400).send({ message: 'Error round number not specified.' })
  }
  await set("current_challenge", challenge_id);
  await set("number_of_bags", defaultNumberOfBags);
  await set("round_number", round_number);
  setChallengeToClass(challenge_id);

  await initializeBagsPoints(defaultNumberOfBags);
  if (users.length == 0) {
    users = generateDummyFitboxers(defaultNumberOfBags);
  }
  await hset('fitboxers', { 'data': JSON.stringify(users) });

  try {
    const resultClass = await query('SELECT * from classes where  status = ?  ORDER by created_at desc  ', ['STARTED']);
    if (resultClass.length == 0) {
      res.json({ status: false, msgcode: 'NOCLASS', message: 'No class available' });
      return;
    }
    const class_id = resultClass[0].id;
    const resultRound = await query('SELECT * from rounds where class_id = ? and round_number = ? ', [class_id, round_number]);
    if (resultRound.length > 0) {
      await query("DELETE FROM bag_rounds where round_id in (SELECT id FROM rounds where class_id=? and round_number=? )", [class_id, round_number]);
      await query("DELETE FROM rounds where  class_id = ? and round_number = ?", [class_id, round_number]);
      console.log("Round " + round_number + " created again for class:" + class_id);
    }
    let dataRound = [new Date(), new Date(), class_id, round_number, video_id, challenge_id, start_time_local, "STARTED"];
    let sqlInsertRound = 'INSERT INTO rounds(created_at,updated_at,class_id,round_number,video_id,challenge_id,start_time_local,status) VALUES (   ? ) ';
    let round_id = 0;
    try {
      const insertRoundResult = await query(sqlInsertRound, [dataRound])
      round_id = insertRoundResult.insertId;
      console.log(`Round ${round_number} created successfully, round_id:${round_id}`);
    } catch (err) {
      console.error(`Failed to save data in round table for round ${round_number}, error:`, err.message);
      throw err;
    }
    try {
      const usersRows = [];
      users.forEach(function (user) {
        usersRows.push([new Date(), new Date(), round_id, user.sb, user.nick, user.gender, user.workout, user.power, user.photo, user.logo, user.fbp_mult, user.pwp_mult])
      });
      const bagRoundResult = await query('INSERT INTO bag_rounds(created_at,updated_at,round_id,bag_number,user_nick,user_gender,user_workout,user_power,user_photo,user_logo,fbp_mult,pwp_mult) VALUES  ? ', [usersRows])
      // set mac in bag rounds using bag_number
      let updateMacSQL = 'UPDATE bag_rounds a  JOIN bags b ON a.bag_number = b.bag_number   SET a.mac = b.mac where a.mac is null and round_id = ?  ;';
      await query(updateMacSQL, [round_id]);
      res.send({ message: "Round " + round_number + " , created succesfully," + bagRoundResult.affectedRows + "  fitboxers registered ", data: { class_id, round_id, status: true } });
      return;
    } catch (errorBR) {
      console.error(`Failed to save bag_rounds for round ${round_number}, error:`, errorBR.message);
      throw errorBR
    }
  } catch (err) {
    console.error(`Fail to start round ${round_number}`);
    console.error(err);
  }
});

router.post('/:round_number/finish/', async function (req, res, next) {
  let round_number = parseInt(req.params.round_number);
  let status = 'FINISHED';
  var resultClass = await query("SELECT * from classes where status = ? ORDER BY created_at desc", ['STARTED']);
  if (resultClass.length === 0) {
    res.status(400).json({ msgcode: 'NOCLASS', message: 'No class available to finish', status: false })
    return
  }
  let class_id = resultClass[0].id
  var resultRoundId = await query("SELECT id from rounds where class_id=? and round_number=?", [class_id, round_number]);

  if (resultRoundId[0] == null) {
    res.json({ message: 'Round ' + round_number + ' has not started yet' })
    return;
  }
  let round_id = resultRoundId[0].id;
  var fitboxers = await hgetall('fitboxers')

  fitboxers = JSON.parse(fitboxers.data);
  const bagsList = await hgetall('workout:bags');
  for (var i = 0; i < fitboxers.length; i++) {

    let fitboxer = fitboxers[i];
    var redisBagPoints = bagsList[fitboxer.sb];
    redisBagPoints = redisBagPoints.split(":");
    let hits = parseInt(redisBagPoints[0]);
    let fbp = parseInt(redisBagPoints[1]);
    let pwp = parseInt(redisBagPoints[2]);
    let totp = parseInt(redisBagPoints[3]);
    let workout = parseFloat(redisBagPoints[4]) * 100;
    let power = parseFloat(redisBagPoints[5]);
    var sqlUpdate = "UPDATE bag_rounds set  "
      + " hits=" + hits
      + ",fbp=" + fbp
      + ",pwp=" + pwp
      + ",totp=" + totp
      + ",workout=" + workout
      + ",power=" + power
      + "  WHERE bag_number=" + fitboxer.sb + " and round_id=" + round_id;

    await mysql.query(sqlUpdate);

  }
  await query('UPDATE rounds set status = ?,updated_at = NOW()  where class_id= ? ', [status, class_id])
  res.send({ status: true, message: 'Round ' + round_number + ' finished succesfully' });
  // const fitzone_id = await get('fitzone_id')
  // console.log(`Sending hits details to fitzone for round:${round_number}, fitzone_id:${fitzone_id}`)
  // if (parseInt(fitzone_id) != 1) {
  //   sendHitsDetail(fitzone_id, round_number)
  // }
});

const generateDummyFitboxers = (qty) => {
  const result = [];
  for (let i = 1; i < qty / 2; i++) {
    result.push({
      sb: i,
      nick: "",
      gender: "F",
      workout: 0,
      power: 0,
      photo: "woman_ico.png",
      logo: "",
      fbp_mult: 0,
      pwp_mult: 0
    });
  }
  for (let i = qty / 2; i <= qty.length; i++) {
    result.push({
      sb: i,
      nick: "",
      gender: "M",
      workout: 0,
      power: 0,
      photo: "woman_ico.png",
      logo: "",
      fbp_mult: 0,
      pwp_mult: 0
    });
  }
  return result;
};

module.exports = router;