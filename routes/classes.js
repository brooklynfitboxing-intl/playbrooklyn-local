const util = require('util');
var express = require('express');
var router = express.Router();
const mysql = require('../data/config');
const query = util.promisify(mysql.query).bind(mysql);
const { fitzone, fwgServer } = require('../data/axios');
const { set } = require('../data/redis');
const { exec } = require('child_process');

router.get('/', function (req, res, next) {
  mysql.query('SELECT * FROM classes', (error, result) => {
    if (error) throw error;
    res.send(result);
  });
});

router.post('/:fitzone_id/create', async function (req, res, next) {
  const { fitzone_id } = req.params;
  let data = ['STARTED', new Date(), new Date(), fitzone_id];
  let sqlClass = "SELECT * from classes where fitzone_id = ? ";
  var result = await query(sqlClass, [fitzone_id]);
  await set('fitzone_id', fitzone_id)
  
  if (result.length) {
    console.log(`Session with fitzone id: ${fitzone_id}, already exists, cleaning existing data`)
    result.forEach(async (item) => {
      await query("DELETE FROM bag_rounds where round_id in (SELECT id FROM rounds where class_id=?)", item.id);
      await query("DELETE FROM rounds where class_id = ?", item.id);
      await query("DELETE FROM classes where id = ?", item.id);
    });
  }
  result = await query('INSERT INTO classes(status, created_at, updated_at, fitzone_id) VALUES ( ? ) ', [data])
  if (parseInt(fitzone_id) === 1) {
    console.log('DUMMY session created')
  } else {
    console.log(`Session ${fitzone_id} created sucessfuly`)
  }
  res.send({ status: true, message: "Class fitzone_id:" + fitzone_id + " created succesfully", data: { class_id: result.insertId } });
});

router.post('/:fitzone_id/finish/', async function (req, res, next) {
  const { fitzone_id } = req.params
  const status = 'FINISHED';
  try {
    const result = await query('SELECT * from classes where fitzone_id = ? ', [fitzone_id])
    if (result.length === 0) {
      res.send({ status: false, message: 'Could not found class with fitzone_id ' + fitzone_id });
    } else {
      await query('UPDATE classes set status = ?,updated_at = NOW(),finish_at = NOW()  where fitzone_id =?   ', [status, fitzone_id])
    }
    res.send({ status: true, message: "Class " + fitzone_id + " finished succesfully " });
    if (parseInt(fitzone_id) === 1) {
      console.log('DUMMY session finished')
    } else {
      console.log(`Session ${fitzone_id} finished sucessfuly`)
    }
  } catch (err) {
    console.error(`Could not finish class with fitzone_id: ${fitzone_id}`, err)
    res.status(500).json({ status: false, message: 'Could not found class with fitzone_id ' + fitzone_id });
  }
  return
});

router.get('/:fitzone_id/final_results', async function (req, res, next) {
  let fitzone_id = req.params.fitzone_id;
  let sqlSummary = 'SELECT bag_number, (SUM(workout)/8)/100 AS energy, (SUM(power)/8) as pow, user_nick as nick,user_photo avatar FROM bag_rounds join rounds on rounds.id = round_id  where  class_id in ( SELECT id  from classes where fitzone_id = ? ) GROUP by bag_number, user_nick, user_photo  ORDER by bag_number;';
  try {
    let fitboxersSummary = await query(sqlSummary, [fitzone_id]);
    res.send({ status: true, data: fitboxersSummary });
  } catch (err) {
    console.error(err);
    res.status(500).send({ status: false, message: "Error trying to get final results", error: err });
    return;
  }
});

const getSessionResults = async function (session, challengeDescription) {
  const classResult = [];
  const rounds = await query("SELECT * from rounds where class_id = ? order by round_number asc", [session.id]);
  for (const round of rounds) {
    const roundResult = {}
    const bagsRounds = await query(" SELECT * from bag_rounds where round_id = ?  ", [round.id]);
    const roundNumberStr = round.round_number.toString().padStart(2, "0");
    const roundDescription = (round.round_number * 3).toString().padStart(2, "0") + '. ' + challengeDescription + ' ROUND' + roundNumberStr
    roundResult['round'] = roundDescription;
    roundResult['scores'] = [];
    for (const round_bag of bagsRounds) {
      var score = {};
      score["mac"] = round_bag["mac"];
      score["sb"] = round_bag["bag_number"];
      score["hits"] = round_bag["hits"];
      score["fbp"] = round_bag["fbp"];
      score["pwp"] = round_bag["pwp"];
      score["totp"] = round_bag["totp"];
      score["workout"] = round_bag["workout"] / 100;
      score["power"] = round_bag["power"];
      roundResult.scores.push(score);
    }
    classResult.push(roundResult);
  }
  return classResult;
}


router.get('/end_class/:challenge/:sessionId ', async function (request, res, next) {
  const { sessionId, challenge } = request.params
  const classes = await query("SELECT id, created_at, sent_to_fitzone, fitzone_id, challenge from classes where id = ? ", [sessionId]);
  for (const session of classes) {
    const response = await getSessionResults(session, challenge)
    const dataToFitzone = JSON.stringify({ id: session.fitzone_id, results: response });
    try {
      const resp = await fitzone.post('/end_class/', { id: session.fitzone_id, json: dataToFitzone })
      console.log('Session:' + session.id + ' sent succesfully');
      await query('UPDATE classes set sent_to_fitzone = 1, json_to_fitzone=? where id = ?', [dataToFitzone, session.id]);
      classesResponses.push({ session, response: resp.data })
    } catch (error) {
      console.error('Could not send session:' + session.id, error);
      await query('UPDATE classes set sent_to_fitzone = 0, json_to_fitzone=? where id = ?', [dataToFitzone, session.id]);
    }
  }
  res.send({ status: true, message: 'sessions sent to fitzone: ' + classesResponses.length, data: classesResponses });
})

router.get('/send_results/:challenge_description/', async function (request, res, next) {
  const challengeDescription = request.params.challenge_description
  const classes = await query("SELECT id, created_at, sent_to_fitzone, fitzone_id from classes where status='FINISHED' and  sent_to_fitzone = 0 ");
  console.debug("Pending classes to send:", classes.length, classes)
  var classesResponses = [];
  for (const session of classes) {
    const response = await getSessionResults(session, challengeDescription)
    var dataToFitzone = JSON.stringify({ id: session.fitzone_id, results: response });
    try {
      const resp = await fitzone.post('/end_class/', { id: session.fitzone_id, json: dataToFitzone })
      console.log('Session:' + session.id + ' sent succesfully');
      await query('UPDATE classes set sent_to_fitzone = 1, json_to_fitzone=? where id = ?', [dataToFitzone, session.id]);
      classesResponses.push({ session, response: resp.data })
    } catch (error) {
      console.error('Could not send session:' + session.id, error);
      await query('UPDATE classes set sent_to_fitzone = 0, json_to_fitzone=? where id = ?', [dataToFitzone, session.id]);
    }
  }
  res.send({ status: true, message: 'sessions sent to fitzone: ' + classesResponses.length, data: classesResponses });
  exec(`redis-cli --scan --pattern hits* | xargs redis-cli del`)
});

router.get('/send_combats/:tournamentId/:combatTime/:roundNumber/:roundId', async function (request, res, next) {
  const { roundId, tournamentId, combatTime } = request.params;
  const roundNumber = parseInt(request.params.roundNumber);
  const roundResult = {}
  console.log(`send_combat info: combatTime ${combatTime}, roundId: ${roundId}, roundNumber:${roundNumber}`)
  console.log("Sending round information to server:");
  const bagsRounds = await query("SELECT * from bag_rounds where round_id = ?  ", [roundId]);
  roundResult['round'] = roundNumber;
  roundResult['scores'] = [];
  for (const round_bag of bagsRounds) {
    const score = {};
    score["mac"] = round_bag["mac"];
    score["sb"] = round_bag["bag_number"];
    score["hits"] = round_bag["hits"];
    score["fbp"] = round_bag["fbp"];
    score["pwp"] = round_bag["pwp"];
    score["totp"] = round_bag["totp"];
    score["workout"] = round_bag["workout"] / 100;
    score["power"] = round_bag["power"];
    roundResult.scores.push(score);
  }
  console.log(`Total bags scores to send: ${bagsRounds.length}`)
  console.log(`scores detail to send `, roundResult.scores)
  try {
    const res = await fwgServer.post(`/player/${tournamentId}/results/${combatTime}`, { round: roundResult['round'], scores: JSON.stringify(roundResult['scores']) });
    console.log('reponse: ' + res.statusText);
  } catch (error) {
    console.error('ha ocurrido un error', error.response)
    console.error(error.response)
  }
  res.send({ status: true, message: "resultados enviados" });
});


module.exports = router;
