let items = {
  items: [
    {
      "name": "debuff",
      "value": 0.5      
    },
    {
      "name": "buff",
      "value": 0.5      
    },
    {
      "name": "debuff",
      "value": 1      
    },
    {
      "name": "buff",
      "value": 1      
    },
    {
      "name": "forward",
      "value": 3      
    },
    {
      "name": "backward",
      "value": 3      
    },
    {
      "name": "forward",
      "value": 4      
    },
    {
      "name": "backward",
      "value": 4      
    },
  ]
}

var express = require('express');
var router = express.Router();

const randomGrid = function (items) {
  let table = []
  let keys = Object.keys(items);
  for (let i = 0; i <= 23; i++) {
    table.push(items[keys[ keys.length * Math.random() << 0]]);
  }
  return table;
};

router.get('/getTable/', (req,res) => {
  let response = randomGrid(items.items)
  res.send(response)
})

module.exports = router;