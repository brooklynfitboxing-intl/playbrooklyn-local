const express = require('express');
const { fwgServer } = require('../data/axios');
const router = express.Router();
const { get, set, hgetall } = require("../data/redis");
const { setBagColor } = require('../services/bags');

const getCombats = async (tournamentId) => {
    const combatsResponse = await fwgServer.get(`/player/${tournamentId}/combats/`);
    const combats = combatsResponse.data;
    combats.sort((a, b) => a.arena - b.arena)
    return combats
}

const nextCombats = async (req, res, next) => {
    const { tournamentId } = req.params;
    try {
        await set('current_tournament', tournamentId)
        const combats = await getCombats(tournamentId)
        await set('current_combats', JSON.stringify(combats))
        return res.send(combats)
    } catch (err) {
        const msgErr = err.response ? err.response.data : err 
        console.log(msgErr)
        return res.status(500).json({message: `Error getting tournament data from fwg server: ${msgErr}`})
    }
}

const combats = async (req, res, next) => {
    const arena = parseInt(req.params.arena);
    const combats = JSON.parse(await get('current_combats'));
    let result = combats;
    if (arena) {
        result = combats.filter(combat => combat.arena === arena);
    }
    result.sort((a, b) => a.arena - b.arena)
    return res.send(result)
}

const combatsParticipants = async (req, res, next) => {
    const arena = parseInt(req.params.arena);
    try {
        const combats = JSON.parse(await get('current_combats'));
        let resultParticipants = [];
        if (arena === 0) {
            const allParticipants = []
            await Promise.all(
                combats.map(async (comb) => {
                    const participantsResponse = await fwgServer.get(`/player/participants/${comb.id}`);
                    allParticipants.push(participantsResponse.data)
                })
            )
            allParticipants.forEach(it => {
                resultParticipants = resultParticipants.concat(it.team1, it.team2)
            })
        } else {
            if (arena > 5) {
                return res.status(400).json({ message: `Invalid arena` });
            }
            const combatArena = combats.find(combat => combat.arena === arena);
            if (!combatArena) {
                return res.status(404).json({ message: `No combat found for arena ${arena}` });
            }
            const response = await fwgServer.get(`/player/participants/${combatArena.id}`);
            resultParticipants = response.data
        }
        return res.send(resultParticipants);
    } catch (err) {
        console.error(err.response ? err.response.data : err ) 
        res.status(500).send(err.response ? err.response.data : err)
    }
}

const setCurrentView = async (req, res, next) => {
    const { view } = req.body;
    await set('current_view', view)
    return res.send('OK')
}

const getCurrentView = async (req, res, next) => {
    const current_view = await get('current_view')
    return res.send({ status: true, current_view })
}

const getComboResults = async (req, res) => {
    const { tournamentId, combatTime, comboNumber, arena } = req.params;
    try {
        const scoresResponse = await fwgServer.get(`/player/${tournamentId}/scores/${combatTime}/${comboNumber}`);
        if (arena) {
            const result = scoresResponse.data.filter(it => it.currentCombo.arena === parseInt(arena))
            return res.send(result[0])
        }
        return res.send(scoresResponse.data)
    } catch (ex) {
        console.error(ex)
        return res.status(500).send(ex.message)
    }
}

const setWinnerLight = async (req, res) => {
    const { teamWinner } = req.body
    const { arena } = req.params
    if (!teamWinner) {
        res.status(400).json({ message: 'No team specified' })
        return
    } 
    console.log(`winners, arena:${arena}, winner: [team${teamWinner}]`)
    const combats = JSON.parse(await get('current_combats'));
    const combat  = combats.filter(combat => combat.arena === parseInt(arena))[0]
    const team = `team${teamWinner}`
    console.log('combat', combat)
    const participants  = combat.participants[team]
    const bagsWinner = participants.filter(it => it.bag > 0).map(it => it.bag)
    console.log('bagsWinner', participants)
    const color = parseInt(teamWinner) === 1 ? 'TEAM_A' : 'TEAM_B'
    await Promise.all( bagsWinner.map( bag => {
        return setBagColor(bag, color)
    }))
    res.send('Ok')
    return
}

const getSummary = async (req, res) => {
    const { arena } = req.params;
    try {
        const tournamentId = await get('current_tournament')
        let combat = await get('current_combats')
        if (combat === null) {
            combat = await getCombats(tournamentId)
        } else {
            combat = JSON.parse(combat)
        }
        combat = combat[0]
        const scoresResponse = await fwgServer.get(`/player/${tournamentId}/scores/${combat.time}/${combat.lastCombo}`);
        if (arena) {
            const result = scoresResponse.data.filter(it => it.currentCombo.arena === parseInt(arena))
            return res.send(result)
        }
        return res.send(scoresResponse.data)
    } catch (ex) {
        console.error(ex)
        return res.status(500).send(ex.message)
    }
}

const getRealTimeResults = async (req, res, next) => {
    let result = [];
    const fitboxers = await hgetall('fitboxers')
    const round_id = await get('round_id')
    let bagsList = await hgetall('workout:bags')
    if (bagsList === null) {
        bagsList = query('SELECT * from bag_rounds  where  round_id = ? ', [round_id]);
    }
    if (fitboxers === null) {
        for (const bag in bagsList) {
            const bg = bagsList[bag];
            result.push({
                sb: parseInt(bag),
                hits: bg.user_hits,
                fbp: bg.user_fbp,
                pwp: bg.user_pwp,
                totp: bg.user_totp,
                workout: bg.user_workout,
                power: bg.user_power,
            })
        }
    } else {
        result = JSON.parse(fitboxers.data);
        result.sort((a, b) => a.sb - b.sb)
        //retorna puntuacion para los sacos y fitboxers en el round actual
        result.forEach(function (fitboxer) {
            let redisBagPoints = bagsList[fitboxer.sb];
            if (redisBagPoints) {
                redisBagPoints = redisBagPoints.split(":");
                let hits = parseInt(redisBagPoints[0]);
                let fbp = parseInt(redisBagPoints[1]);
                let pwp = parseInt(redisBagPoints[2]);
                let totp = parseInt(redisBagPoints[3]);
                let workout = parseFloat(redisBagPoints[4]);
                let power = parseFloat(redisBagPoints[5]);

                workout = workout > 0 ? workout.toFixed(3) : 0;
                power = power > 0 ? power.toFixed(1) : 0;
                fitboxer.workout = workout;
                fitboxer.power = power;
                fitboxer.pwp = pwp;
                fitboxer.hits = hits;
                fitboxer.fbp = fbp;
                fitboxer.totp = totp;
            }
        });
    }
    return res.send({ data: result });
}
const currentTournament = async (req, res) => {
    return res.send(await get('current_tournament'))
}
router.get('/', currentTournament);
router.get('/:tournamentId/next_combats/', nextCombats);
router.get('/:tournamentId/scores/:combatTime/:comboNumber?/:arena?', getComboResults)

router.get('/real_time_results/:arena?', getRealTimeResults);

router.get('/current_view', getCurrentView);
router.post('/current_view', setCurrentView);

router.get('/combats/:arena?', combats);
router.get('/combats/:arena/participants', combatsParticipants);
router.get('/combats/:arena/summary', getSummary);

router.post('/combats/:arena/set_winners', setWinnerLight);

module.exports = router;