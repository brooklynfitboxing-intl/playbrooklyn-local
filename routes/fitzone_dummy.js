/* let challenges = [
  {
    "id":1,
    "description": "BR149",
    "name": "BF Challenge 150",
    "information": "Challenge 150 Tus piernas van a trabajar de lo lindo con el #BFChallenge145. Flexiona las rodillas al ejecutar la esquiva circular para que tus glúteos y cuádriceps estén a tope. Si quieres seguir tonificando tus brazos, mantén el brazo que no ejecuta el golpe con el codo cerca de las costillas y el puño pegado a la mandíbula",
    "image": "https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2020/05/149TM.jpg",
    "preview": "https://www.youtube.com/embed/V12THIH1paA"
  },
  {
    "id":2,
    "name": "BF Challenge 148",
    "description": "BR148",
    "information": "Challenge 148 Tus piernas van a trabajar de lo lindo con el #BFChallenge145. Flexiona las rodillas al ejecutar la esquiva circular para que tus glúteos y cuádriceps estén a tope. Si quieres seguir tonificando tus brazos, mantén el brazo que no ejecuta el golpe con el codo cerca de las costillas y el puño pegado a la mandíbula",
    "image": "https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2020/04/BFChallenge145_Moves.jpg",
    "preview": "https://www.youtube.com/embed/V12THIH1paA"
  },
  {
    "id":3,
    "name": "BF Challenge 152",
    "description": "BR152",
    "information": "Challenge 147 Tus piernas van a trabajar de lo lindo con el #BFChallenge145. Flexiona las rodillas al ejecutar la esquiva circular para que tus glúteos y cuádriceps estén a tope. Si quieres seguir tonificando tus brazos, mantén el brazo que no ejecuta el golpe con el codo cerca de las costillas y el puño pegado a la mandíbula",
    "image": "https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2019/12/138_Moves.jpg",
    "preview": "https://www.youtube.com/embed/V12THIH1paA",
    "IMG_URL": "https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2019/12/138_Moves.jpg",
    "ID":"BR152",
    "DESC_ES":"Challenge 152 Tus piernas van a trabajar de lo lindo con el #BFChallenge145. Flexiona las rodillas al ejecutar la esquiva circular para que tus glúteos y cuádriceps estén a tope. Si quieres seguir tonificando tus brazos, mantén el brazo que no ejecuta el golpe con el codo cerca de las costillas y el puño pegado a la mandíbula"
  },
  {
    "id":4,
    "name": "BF Challenge 153",
    "description": "BR153",
    "information": "Challenge 149 Tus piernas van a trabajar de lo lindo con el #BFChallenge145. Flexiona las rodillas al ejecutar la esquiva circular para que tus glúteos y cuádriceps estén a tope. Si quieres seguir tonificando tus brazos, mantén el brazo que no ejecuta el golpe con el codo cerca de las costillas y el puño pegado a la mandíbula",
    "image": "https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2020/03/challenge143.jpg",
    "preview": "https://www.youtube.com/embed/V12THIH1paA",
    "IMG_URL": "https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2020/03/challenge143.jpg",
    "ID":"BR153",
    "DESC_ES":"Challenge 153 Tus piernas van a trabajar de lo lindo con el #BFChallenge145. Flexiona las rodillas al ejecutar la esquiva circular para que tus glúteos y cuádriceps estén a tope. Si quieres seguir tonificando tus brazos, mantén el brazo que no ejecuta el golpe con el codo cerca de las costillas y el puño pegado a la mandíbula"
  }
];
 */

/* const data = {
   "numberOfBags":20,
   "proyectorDelay":150,
   "challenges":[
    {
     "id":"BR191",
     "name":
     "Challenge #191",
     "published":true,
     "from":"2022-01-02T23:00:00.000Z",
     "to":"2022-01-15T23:00:00.000Z",
     "vid_url":"https://www.youtube.com/embed/P8msb3ABy90",
     "vid_tec_url":"https://www.youtube.com/embed/JqZrnuIZad0",
     "img_url":"https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2021/12/CHEAT_SEAT_-191.jpeg",
     "desc_es":"Ahora que ya tenemos todos los golpes estudiados\nEs el turno de la SLIP\n\nEl #BFChallenge191 es pura diversión y viene cargado de SLIP para que te salgan perfectas",
     "desc_en":"Now that we’ve studied all the hits\nIt’s time for the SLIP\n\nThe #BFChallenge191 is pure fun and is charged with SLIPS so you can perfect them",
     "desc_it":"Ora che abbiamo studiato tutti i colpi\nÈ giunto il momento di fare SLIPPING\nLa #BFChallenge191 è puro divertimento ed è carica di SLIPS in modo da poterli perfezionare",
     "desc_ru":"Теперь, когда мы изучили все удары\nПришло время для УКЛОНА\n\n#BFChallenge191 - чисто счастье, в котором есть УКЛОНЫ, чтобы вы смогли их усовершенствовать",
     "calories":824,
     "duration":47,
     "techMoves":"",
     "trainer":"",
     "cover":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/images/Cover191_BR.jpg",
     "isActive":true,
     "videos":[
       {
       "id":"61cccd4124aa9a00070b9cc0",
       "name":"WarmUp191",
       "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/01. WarmUp191.mp4",
       "checksum":"\"e653be6b159e46ab3ece385457ffa602-33\"",
       "type":{
         "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
         "name":"WARM_UP"}
        },
        {
          "id":"61cccd6724aa9a00070b9ccd",
          "name":"GlovesOn",
          "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/02. GlovesOn.mov",
          "checksum":"\"0c6d57ed94949cd75f980b3c0df20cbf\"",
          "type":{
            "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            "name":"OTHERS"
          }
        }]
      },
      {
        "id":"BR192",
        "name":
        "Challenge #191",
        "published":true,
        "from":"2022-01-02T23:00:00.000Z",
        "to":"2022-01-15T23:00:00.000Z",
        "vid_url":"https://www.youtube.com/embed/P8msb3ABy90",
        "vid_tec_url":"https://www.youtube.com/embed/JqZrnuIZad0",
        "img_url":"https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2021/12/CHEAT_SEAT_-191.jpeg",
        "desc_es":"Ahora que ya tenemos todos los golpes estudiados\nEs el turno de la SLIP\n\nEl #BFChallenge191 es pura diversión y viene cargado de SLIP para que te salgan perfectas",
        "desc_en":"Now that we’ve studied all the hits\nIt’s time for the SLIP\n\nThe #BFChallenge191 is pure fun and is charged with SLIPS so you can perfect them",
        "desc_it":"Ora che abbiamo studiato tutti i colpi\nÈ giunto il momento di fare SLIPPING\nLa #BFChallenge191 è puro divertimento ed è carica di SLIPS in modo da poterli perfezionare",
        "desc_ru":"Теперь, когда мы изучили все удары\nПришло время для УКЛОНА\n\n#BFChallenge191 - чисто счастье, в котором есть УКЛОНЫ, чтобы вы смогли их усовершенствовать",
        "calories":824,
        "duration":47,
        "techMoves":"",
        "trainer":"",
        "cover":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/images/Cover191_BR.jpg",
        "isActive":true,
        "videos":[
          {
          "id":"61cccd4124aa9a00070b9cc0",
          "name":"WarmUp191",
          "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/01. WarmUp191.mp4",
          "checksum":"\"e653be6b159e46ab3ece385457ffa602-33\"",
          "type":{
            "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
            "name":"WARM_UP"}
           },
           {
             "id":"61cccd6724aa9a00070b9ccd",
             "name":"GlovesOn",
             "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/02. GlovesOn.mov",
             "checksum":"\"0c6d57ed94949cd75f980b3c0df20cbf\"",
             "type":{
               "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
               "name":"OTHERS"
             }
           }]
         }
    ],
    "games":[
      {
       "id":"Game1",
       "name":
       "Game #191",
       "published":true,
       "from":"2022-01-02T23:00:00.000Z",
       "to":"2022-01-15T23:00:00.000Z",
       "vid_url":"https://www.youtube.com/embed/P8msb3ABy90",
       "vid_tec_url":"https://www.youtube.com/embed/JqZrnuIZad0",
       "img_url":"https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2021/12/CHEAT_SEAT_-191.jpeg",
       "desc_es":"Ahora que ya tenemos todos los golpes estudiados\nEs el turno de la SLIP\n\nEl #BFChallenge191 es pura diversión y viene cargado de SLIP para que te salgan perfectas",
       "desc_en":"Now that we’ve studied all the hits\nIt’s time for the SLIP\n\nThe #BFChallenge191 is pure fun and is charged with SLIPS so you can perfect them",
       "desc_it":"Ora che abbiamo studiato tutti i colpi\nÈ giunto il momento di fare SLIPPING\nLa #BFChallenge191 è puro divertimento ed è carica di SLIPS in modo da poterli perfezionare",
       "desc_ru":"Теперь, когда мы изучили все удары\nПришло время для УКЛОНА\n\n#BFChallenge191 - чисто счастье, в котором есть УКЛОНЫ, чтобы вы смогли их усовершенствовать",
       "calories":824,
       "duration":47,
       "techMoves":"",
       "trainer":"",
       "cover":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR190/images/Cover190_BR.jpg",
       "isActive":true,
       "videos":[
         {
         "id":"61cccd4124aa9a00070b9cc0",
         "name":"WarmUp191",
         "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/01. WarmUp191.mp4",
         "checksum":"\"e653be6b159e46ab3ece385457ffa602-33\"",
         "type":{
           "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
           "name":"WARM_UP"}
          },
          {
            "id":"61cccd6724aa9a00070b9ccd",
            "name":"GlovesOn",
            "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/02. GlovesOn.mov",
            "checksum":"\"0c6d57ed94949cd75f980b3c0df20cbf\"",
            "type":{
              "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
              "name":"OTHERS"
            }
          }]
        },
        {
          "id":"Game2",
          "name":
          "Game #192",
          "published":true,
          "from":"2022-01-02T23:00:00.000Z",
          "to":"2022-01-15T23:00:00.000Z",
          "vid_url":"https://www.youtube.com/embed/P8msb3ABy90",
          "vid_tec_url":"https://www.youtube.com/embed/JqZrnuIZad0",
          "img_url":"https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2021/12/CHEAT_SEAT_-191.jpeg",
          "desc_es":"Ahora que ya tenemos todos los golpes estudiados\nEs el turno de la SLIP\n\nEl #BFChallenge191 es pura diversión y viene cargado de SLIP para que te salgan perfectas",
          "desc_en":"Now that we’ve studied all the hits\nIt’s time for the SLIP\n\nThe #BFChallenge191 is pure fun and is charged with SLIPS so you can perfect them",
          "desc_it":"Ora che abbiamo studiato tutti i colpi\nÈ giunto il momento di fare SLIPPING\nLa #BFChallenge191 è puro divertimento ed è carica di SLIPS in modo da poterli perfezionare",
          "desc_ru":"Теперь, когда мы изучили все удары\nПришло время для УКЛОНА\n\n#BFChallenge191 - чисто счастье, в котором есть УКЛОНЫ, чтобы вы смогли их усовершенствовать",
          "calories":824,
          "duration":47,
          "techMoves":"",
          "trainer":"",
          "cover":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR190/images/Cover190_BR.jpg",
          "isActive":true,
          "videos":[
            {
            "id":"61cccd4124aa9a00070b9cc0",
            "name":"WarmUp191",
            "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/01. WarmUp191.mp4",
            "checksum":"\"e653be6b159e46ab3ece385457ffa602-33\"",
            "type":{
              "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
              "name":"WARM_UP"}
             },
             {
               "id":"61cccd6724aa9a00070b9ccd",
               "name":"GlovesOn",
               "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/02. GlovesOn.mov",
               "checksum":"\"0c6d57ed94949cd75f980b3c0df20cbf\"",
               "type":{
                 "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
                 "name":"OTHERS"
               }
             }]
           },
           {
            "id":"Game3",
            "name":
            "Game #192",
            "published":true,
            "from":"2022-01-02T23:00:00.000Z",
            "to":"2022-01-15T23:00:00.000Z",
            "vid_url":"https://www.youtube.com/embed/P8msb3ABy90",
            "vid_tec_url":"https://www.youtube.com/embed/JqZrnuIZad0",
            "img_url":"https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2021/12/CHEAT_SEAT_-191.jpeg",
            "desc_es":"Ahora que ya tenemos todos los golpes estudiados\nEs el turno de la SLIP\n\nEl #BFChallenge191 es pura diversión y viene cargado de SLIP para que te salgan perfectas",
            "desc_en":"Now that we’ve studied all the hits\nIt’s time for the SLIP\n\nThe #BFChallenge191 is pure fun and is charged with SLIPS so you can perfect them",
            "desc_it":"Ora che abbiamo studiato tutti i colpi\nÈ giunto il momento di fare SLIPPING\nLa #BFChallenge191 è puro divertimento ed è carica di SLIPS in modo da poterli perfezionare",
            "desc_ru":"Теперь, когда мы изучили все удары\nПришло время для УКЛОНА\n\n#BFChallenge191 - чисто счастье, в котором есть УКЛОНЫ, чтобы вы смогли их усовершенствовать",
            "calories":824,
            "duration":47,
            "techMoves":"",
            "trainer":"",
            "cover":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR190/images/Cover190_BR.jpg",
            "isActive":true,
            "videos":[
              {
              "id":"61cccd4124aa9a00070b9cc0",
              "name":"WarmUp191",
              "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/01. WarmUp191.mp4",
              "checksum":"\"e653be6b159e46ab3ece385457ffa602-33\"",
              "type":{
                "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
                "name":"WARM_UP"}
               },
               {
                 "id":"61cccd6724aa9a00070b9ccd",
                 "name":"GlovesOn",
                 "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/02. GlovesOn.mov",
                 "checksum":"\"0c6d57ed94949cd75f980b3c0df20cbf\"",
                 "type":{
                   "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
                   "name":"OTHERS"
                 }
               }]
             },
             {
              "id":"Game4",
              "name":
              "Game #192",
              "published":true,
              "from":"2022-01-02T23:00:00.000Z",
              "to":"2022-01-15T23:00:00.000Z",
              "vid_url":"https://www.youtube.com/embed/P8msb3ABy90",
              "vid_tec_url":"https://www.youtube.com/embed/JqZrnuIZad0",
              "img_url":"https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2021/12/CHEAT_SEAT_-191.jpeg",
              "desc_es":"Ahora que ya tenemos todos los golpes estudiados\nEs el turno de la SLIP\n\nEl #BFChallenge191 es pura diversión y viene cargado de SLIP para que te salgan perfectas",
              "desc_en":"Now that we’ve studied all the hits\nIt’s time for the SLIP\n\nThe #BFChallenge191 is pure fun and is charged with SLIPS so you can perfect them",
              "desc_it":"Ora che abbiamo studiato tutti i colpi\nÈ giunto il momento di fare SLIPPING\nLa #BFChallenge191 è puro divertimento ed è carica di SLIPS in modo da poterli perfezionare",
              "desc_ru":"Теперь, когда мы изучили все удары\nПришло время для УКЛОНА\n\n#BFChallenge191 - чисто счастье, в котором есть УКЛОНЫ, чтобы вы смогли их усовершенствовать",
              "calories":824,
              "duration":47,
              "techMoves":"",
              "trainer":"",
              "cover":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR190/images/Cover190_BR.jpg",
              "isActive":true,
              "videos":[
                {
                "id":"61cccd4124aa9a00070b9cc0",
                "name":"WarmUp191",
                "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/01. WarmUp191.mp4",
                "checksum":"\"e653be6b159e46ab3ece385457ffa602-33\"",
                "type":{
                  "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
                  "name":"WARM_UP"}
                 },
                 {
                   "id":"61cccd6724aa9a00070b9ccd",
                   "name":"GlovesOn",
                   "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/02. GlovesOn.mov",
                   "checksum":"\"0c6d57ed94949cd75f980b3c0df20cbf\"",
                   "type":{
                     "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
                     "name":"OTHERS"
                   }
                 }]
               },
               {
                "id":"Game5",
                "name":
                "Game #192",
                "published":true,
                "from":"2022-01-02T23:00:00.000Z",
                "to":"2022-01-15T23:00:00.000Z",
                "vid_url":"https://www.youtube.com/embed/P8msb3ABy90",
                "vid_tec_url":"https://www.youtube.com/embed/JqZrnuIZad0",
                "img_url":"https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2021/12/CHEAT_SEAT_-191.jpeg",
                "desc_es":"Ahora que ya tenemos todos los golpes estudiados\nEs el turno de la SLIP\n\nEl #BFChallenge191 es pura diversión y viene cargado de SLIP para que te salgan perfectas",
                "desc_en":"Now that we’ve studied all the hits\nIt’s time for the SLIP\n\nThe #BFChallenge191 is pure fun and is charged with SLIPS so you can perfect them",
                "desc_it":"Ora che abbiamo studiato tutti i colpi\nÈ giunto il momento di fare SLIPPING\nLa #BFChallenge191 è puro divertimento ed è carica di SLIPS in modo da poterli perfezionare",
                "desc_ru":"Теперь, когда мы изучили все удары\nПришло время для УКЛОНА\n\n#BFChallenge191 - чисто счастье, в котором есть УКЛОНЫ, чтобы вы смогли их усовершенствовать",
                "calories":824,
                "duration":47,
                "techMoves":"",
                "trainer":"",
                "cover":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR190/images/Cover190_BR.jpg",
                "isActive":true,
                "videos":[
                  {
                  "id":"61cccd4124aa9a00070b9cc0",
                  "name":"WarmUp191",
                  "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/01. WarmUp191.mp4",
                  "checksum":"\"e653be6b159e46ab3ece385457ffa602-33\"",
                  "type":{
                    "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
                    "name":"WARM_UP"}
                   },
                   {
                     "id":"61cccd6724aa9a00070b9ccd",
                     "name":"GlovesOn",
                     "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/02. GlovesOn.mov",
                     "checksum":"\"0c6d57ed94949cd75f980b3c0df20cbf\"",
                     "type":{
                       "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
                       "name":"OTHERS"
                     }
                   }]
                 },
                 {
                  "id":"Game6",
                  "name":
                  "Game #192",
                  "published":true,
                  "from":"2022-01-02T23:00:00.000Z",
                  "to":"2022-01-15T23:00:00.000Z",
                  "vid_url":"https://www.youtube.com/embed/P8msb3ABy90",
                  "vid_tec_url":"https://www.youtube.com/embed/JqZrnuIZad0",
                  "img_url":"https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2021/12/CHEAT_SEAT_-191.jpeg",
                  "desc_es":"Ahora que ya tenemos todos los golpes estudiados\nEs el turno de la SLIP\n\nEl #BFChallenge191 es pura diversión y viene cargado de SLIP para que te salgan perfectas",
                  "desc_en":"Now that we’ve studied all the hits\nIt’s time for the SLIP\n\nThe #BFChallenge191 is pure fun and is charged with SLIPS so you can perfect them",
                  "desc_it":"Ora che abbiamo studiato tutti i colpi\nÈ giunto il momento di fare SLIPPING\nLa #BFChallenge191 è puro divertimento ed è carica di SLIPS in modo da poterli perfezionare",
                  "desc_ru":"Теперь, когда мы изучили все удары\nПришло время для УКЛОНА\n\n#BFChallenge191 - чисто счастье, в котором есть УКЛОНЫ, чтобы вы смогли их усовершенствовать",
                  "calories":824,
                  "duration":47,
                  "techMoves":"",
                  "trainer":"",
                  "cover":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR190/images/Cover190_BR.jpg",
                  "isActive":true,
                  "videos":[
                    {
                    "id":"61cccd4124aa9a00070b9cc0",
                    "name":"WarmUp191",
                    "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/01. WarmUp191.mp4",
                    "checksum":"\"e653be6b159e46ab3ece385457ffa602-33\"",
                    "type":{
                      "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
                      "name":"WARM_UP"}
                     },
                     {
                       "id":"61cccd6724aa9a00070b9ccd",
                       "name":"GlovesOn",
                       "url":"https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/02. GlovesOn.mov",
                       "checksum":"\"0c6d57ed94949cd75f980b3c0df20cbf\"",
                       "type":{
                         "url_image":"https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
                         "name":"OTHERS"
                       }
                     }]
                   }
      ],
  } */

const data = {
  avatar: "company_ico.png",
  numberOfBags: 20,
  proyectorDelay: 150,
  challenges: [
    {
      id: "BR192",
      name: "Challenge #192",
      published: true,
      from: "2022-01-16T23:00:00.000Z",
      to: "2022-01-29T23:00:00.000Z",
      vid_url: "https://www.youtube.com/embed/CO2MG9wWIxg",
      vid_tec_url: "https://www.youtube.com/embed/FxBqvGXItZs",
      img_url:
        "https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2022/01/cheat_seat_-_192.jpeg",
      desc_es:
        "¿Echabais de menos los challenge cañeros?\n¿Preparados para darlos todo en el saco?\n¡LLEGA EL #BFChallenge192 para que demuestres todo lo que sabes!",
      desc_en:
        "Did you miss the hardcore challenges?\nReady to give your all at the punching bag?\nThe #BFChallenge192 has arrived to help you show off everything you know!",
      desc_it: "",
      desc_ru: "",
      calories: 824,
      duration: 47,
      techMoves:
        "STEP\n· Iniciar con el pie más cercano a la dirección del paso\n· Deslizar, nunca saltar\n· Ambos pies recorren la misma distancia\n· Los brazos se mantienen en la posición de guardia\n· Mantener la separación de pies al final del movimiento\n· Mantener el tronco recto\n· Mantener la barbilla protegida entre los guantes",
      trainer: "Jorge Roglá",
      cover:
        "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/images/Cover192_BR.jpg",
      isActive: true,
      videos: [
        {
          id: "61dffd2824aa9a0007172d43",
          name: "WarmUp192",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/01. WarmUp192.mp4",
          checksum: '"1bd07565e181158e86fe9b348fe156e4-33"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
            name: "WARM_UP",
          },
        },
        {
          id: "61dffdac24aa9a0007172dbc",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/02. GlovesOn.mov",
          checksum: '"0c6d57ed94949cd75f980b3c0df20cbf"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0424aa9a0007172d30",
          name: "R1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/03. BR192 R1.mp4",
          checksum: '"bcbac38d6bfbfb81f4579431f5e812c1-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 1,
            start_in: 17240,
            hits: 64,
            sequence: "X00000X0",
            bpm: 134,
          },
        },
        {
          id: "61dffdac24aa9a0007172dbf",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/04. GlovesOff.mp4",
          checksum: '"4092db1b543b28cab38fd9b4bc959dc6"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0424aa9a0007172d31",
          name: "F1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/05. BR192 F1.mp4",
          checksum: '"5e3ef2dc5149850b8549645fffe12264-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61dffdae24aa9a0007172dc2",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/06. GlovesOn.mp4",
          checksum: '"f3cc6a7f34c3511cea1af7a982c8b2f5-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0424aa9a0007172d2f",
          name: "R2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/07. BR192 R2.mp4",
          checksum: '"c1b4f0569be5f4413154cf46b0ed2d3f-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 2,
            start_in: 17240,
            hits: 192,
            sequence: "XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61dffdac24aa9a0007172dbe",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/08. GlovesOff.mp4",
          checksum: '"26184c123ad3195d8a1a43716bd9dec4"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0124aa9a0007172d2c",
          name: "F2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/09. BR192 F2.mp4",
          checksum: '"49aca297b090c3677d4b9add933d712d-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61dffdae24aa9a0007172dc1",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/10. GlovesOn.mp4",
          checksum: '"04e4b376b8bb3e1f48a44591f953070a-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0324aa9a0007172d2e",
          name: "R3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/11. BR192 R3.mp4",
          checksum: '"1c80ffdb77319964d9efcef1d2fd4c6e-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 3,
            start_in: 17240,
            hits: 128,
            sequence: "X00000X0XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61dffdac24aa9a0007172dbd",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/12. GlovesOff.mp4",
          checksum: '"e49e3582b66554e428e304472d5bb256"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0a24aa9a0007172d33",
          name: "F3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/13. BR192 F3.mp4",
          checksum: '"6c42269fd25ea38d134e668ea8b735e1-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61dffdb224aa9a0007172dc9",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/14. GlovesOn.mp4",
          checksum: '"2bc64c2663416e57d7a93962e1c82883-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0e24aa9a0007172d37",
          name: "R4",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/15. BR192 R4.mp4",
          checksum: '"f537f3b1e8b6280b4c1e485041ff15b0-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 4,
            start_in: 17240,
            hits: 128,
            sequence: "00XXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61dffdae24aa9a0007172dc3",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/16. GlovesOff.mp4",
          checksum: '"bbb92060101273b042e8081d89246e72"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0c24aa9a0007172d35",
          name: "F4",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/17. BR192 F4.mp4",
          checksum: '"a8fac94536a7a52612ce554aae4e1190-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61dffdaf24aa9a0007172dc6",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/18. GlovesOn.mp4",
          checksum: '"f0d9c2ae04f8f396f338efb32c46780e-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0d24aa9a0007172d36",
          name: "R5",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/19. BR192 R5.mp4",
          checksum: '"547779ed93a8d8bb4eb1a10ee3c74b8e-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 5,
            start_in: 17240,
            hits: 192,
            sequence: "XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61dffdae24aa9a0007172dc0",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/20. GlovesOff.mp4",
          checksum: '"10dea1fa3a3c6c7b18edb3893a6f88cb"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd0c24aa9a0007172d34",
          name: "F5",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/21. BR192 F5.mp4",
          checksum: '"88e081fedc4baf3cd5e46a3b35f1c72a-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61dffdb424aa9a0007172dcc",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/22. GlovesOn.mp4",
          checksum: '"9d1bcdb0faf739cb2ef30fa85342ab07-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd1524aa9a0007172d3a",
          name: "R6",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/23. BR192 R6.mp4",
          checksum: '"ae85eb3500140323a24821932cac2194-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 6,
            start_in: 17274,
            hits: 160,
            sequence: "00XXX0X0XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61dffdaf24aa9a0007172dc4",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/24. GlovesOff.mp4",
          checksum: '"944ef4cf997b295274243e476e219d03"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd1424aa9a0007172d38",
          name: "F6",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/25. BR192 F6.mp4",
          checksum: '"1eff48fbd899d88f33cdb863c34f58f1-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61dffdb324aa9a0007172dca",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/26. GlovesOn.mp4",
          checksum: '"c407ef0f524c81c98d8348a82cf9297e-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd1624aa9a0007172d3b",
          name: "R7",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/27. BR192 R7.mp4",
          checksum: '"11cb10522e20568895ba56a50b2dedce-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 7,
            start_in: 17280,
            hits: 144,
            sequence: "X00000X0XXXXX0X000XXX0X0XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61dffdb024aa9a0007172dc7",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/28. GlovesOff.mp4",
          checksum: '"e762280c0300bbab072369703b68b52f"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd1624aa9a0007172d3c",
          name: "F7",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/29. BR192 F7.mp4",
          checksum: '"5d921b405046dd95e25b5265d0cceca7-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61dffdb324aa9a0007172dcb",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/30. GlovesOn.mp4",
          checksum: '"d31a52569ff221c275ae8668e22273d9-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd1b24aa9a0007172d3d",
          name: "R8",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/31. BR192 R8.mp4",
          checksum: '"55a005609f166fe9d62e96ae6c716fb7-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 8,
            start_in: 17280,
            hits: 144,
            sequence: "X00000X0XXXXX0X000XXX0X0XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61dffdb024aa9a0007172dc8",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/32. GlovesOff.mp4",
          checksum: '"cf2c16819ce86196ad1f89cc455fcb26"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61dffd2024aa9a0007172d3e",
          name: "F8",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/33. BR192 F8.mp4",
          checksum: '"fe66a23540a66b5597039c1e6f976008-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61dffd2c24aa9a0007172d44",
          name: "CoolDown192",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/34. CoolDown192.mp4",
          checksum: '"67bce8011aec82917793c77db86d77a8-17"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/cooldown.png",
            name: "COOL_DOWN",
          },
        },
        {
          id: "61e1559024aa9a0007189c45",
          name: "H4C_192",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR192/35. H4C_192.mp4",
          checksum: '"b53eb0b199f4fc8aeb94c3e82fc8eeb8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/h4c_192.png",
            name: "OTHERS",
          },
        },
      ],
      cheatsheet: {
        1: ["Jab", "", "Step", "", "Step", "", "Cross", ""],
        2: ["Jab", "Cross", "Jab", "Cross", "Low Kick", "", "Cross", ""],
        3: "Round 1 + Round 2",
        4: ["Step", "Step", "Upper", "Hook", "Hook", "", "Middle Kick", ""],
        5: ["Jab", "Jab", "Cross", "Cross", "Jab", "", "Cross", ""],
        6: "Round 4 + Round 5",
        7: "Round 1 + Round 2 + Round 4 + Round 5",
        8: "Round 1 + Round 2 + Round 4 + Round 5",
      },
      isDownloaded: true,
    },
    {
      id: "BR191",
      name: "Challenge #191",
      published: true,
      from: "2022-01-02T23:00:00.000Z",
      to: "2022-01-15T23:00:00.000Z",
      vid_url: "https://www.youtube.com/embed/P8msb3ABy90",
      vid_tec_url: "https://www.youtube.com/embed/JqZrnuIZad0",
      img_url:
        "https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2021/12/CHEAT_SEAT_-191.jpeg",
      desc_es:
        "Ahora que ya tenemos todos los golpes estudiados\nEs el turno de la SLIP\n\nEl #BFChallenge191 es pura diversión y viene cargado de SLIP para que te salgan perfectas",
      desc_en:
        "Now that we’ve studied all the hits\nIt’s time for the SLIP\n\nThe #BFChallenge191 is pure fun and is charged with SLIPS so you can perfect them",
      desc_it:
        "Ora che abbiamo studiato tutti i colpi\nÈ giunto il momento di fare SLIPPING\nLa #BFChallenge191 è puro divertimento ed è carica di SLIPS in modo da poterli perfezionare",
      desc_ru:
        "Теперь, когда мы изучили все удары\nПришло время для УКЛОНА\n\n#BFChallenge191 - чисто счастье, в котором есть УКЛОНЫ, чтобы вы смогли их усовершенствовать",
      calories: 824,
      duration: 47,
      techMoves: "",
      trainer: "",
      cover:
        "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/images/Cover191_BR.jpg",
      isActive: false,
      videos: [
        {
          id: "61cccd4124aa9a00070b9cc0",
          name: "WarmUp191",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/01. WarmUp191.mp4",
          checksum: '"e653be6b159e46ab3ece385457ffa602-33"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
            name: "WARM_UP",
          },
        },
        {
          id: "61cccd6724aa9a00070b9ccd",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/02. GlovesOn.mov",
          checksum: '"0c6d57ed94949cd75f980b3c0df20cbf"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd1e24aa9a00070b9cb3",
          name: "R1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/03. BR191 R1.mp4",
          checksum: '"f56b0043f60dd58ef0633e02375851d3-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 1,
            start_in: 17240,
            hits: 64,
            sequence: "00X000X0",
            bpm: 134,
          },
        },
        {
          id: "61cccd6724aa9a00070b9ccc",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/04. GlovesOff.mp4",
          checksum: '"4092db1b543b28cab38fd9b4bc959dc6"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd1824aa9a00070b9cb0",
          name: "F1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/05. BR191 F1.mp4",
          checksum: '"a8ec2229da3c9bcb807b48265ba1b0ea-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61cccd6924aa9a00070b9cd1",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/06. GlovesOn.mp4",
          checksum: '"f3cc6a7f34c3511cea1af7a982c8b2f5-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd1924aa9a00070b9cb1",
          name: "R2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/07. BR191 R2.mp4",
          checksum: '"383c380d07a6094dad6069485894825e-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 2,
            start_in: 17240,
            hits: 192,
            sequence: "XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61cccd6724aa9a00070b9cca",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/08. GlovesOff.mp4",
          checksum: '"26184c123ad3195d8a1a43716bd9dec4"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd1424aa9a00070b9cae",
          name: "F2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/09. BR191 F2.mp4",
          checksum: '"b90b223a6eeb697cf04fbf073c8d85f0-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61cccd6924aa9a00070b9cd2",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/10. GlovesOn.mp4",
          checksum: '"04e4b376b8bb3e1f48a44591f953070a-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd1924aa9a00070b9cb2",
          name: "R3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/11. BR191 R3.mp4",
          checksum: '"7ecf119bb577dca4e341f57d07f76a8d-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 3,
            start_in: 17240,
            hits: 128,
            sequence: "00X000X0XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61cccd6724aa9a00070b9ccb",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/12. GlovesOff.mp4",
          checksum: '"e49e3582b66554e428e304472d5bb256"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd1f24aa9a00070b9cb4",
          name: "F3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/13. BR191 F3.mp4",
          checksum: '"7d056180b44ad37d16a47be15125b801-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61cccd6924aa9a00070b9cd3",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/14. GlovesOn.mp4",
          checksum: '"2bc64c2663416e57d7a93962e1c82883-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd2624aa9a00070b9cb7",
          name: "R4",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/15. BR191 R4.mp4",
          checksum: '"07fc3eae468d54d2f3322069963b6e78-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 4,
            start_in: 17240,
            hits: 128,
            sequence: "X00XX0X0",
            bpm: 134,
          },
        },
        {
          id: "61cccd6824aa9a00070b9cce",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/16. GlovesOff.mp4",
          checksum: '"bbb92060101273b042e8081d89246e72"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd2424aa9a00070b9cb6",
          name: "F4",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/17. BR191 F4.mp4",
          checksum: '"39fb5b22a8d69504e35e3356f4780ab5-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61cccd6a24aa9a00070b9cd7",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/18. GlovesOn.mp4",
          checksum: '"f0d9c2ae04f8f396f338efb32c46780e-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd2724aa9a00070b9cb8",
          name: "R5",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/19. BR191 R5.mp4",
          checksum: '"26bdef34fbfa074aaeafa2c7be1c5dad-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 5,
            start_in: 17240,
            hits: 192,
            sequence: "XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61cccd6824aa9a00070b9ccf",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/20. GlovesOff.mp4",
          checksum: '"10dea1fa3a3c6c7b18edb3893a6f88cb"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd2924aa9a00070b9cb9",
          name: "F5",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/21. BR191 F5.mp4",
          checksum: '"19fdc8d28b75860b69188ab6b49d9f3f-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61cccd6924aa9a00070b9cd4",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/22. GlovesOn.mp4",
          checksum: '"9d1bcdb0faf739cb2ef30fa85342ab07-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd3324aa9a00070b9cbb",
          name: "R6",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/23. BR191 R6.mp4",
          checksum: '"eaefc529d35c9fc7cd849d537a1cf96e-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 6,
            start_in: 17240,
            hits: 160,
            sequence: "X00XX0X0XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61cccd6824aa9a00070b9cd0",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/24. GlovesOff.mp4",
          checksum: '"944ef4cf997b295274243e476e219d03"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd3224aa9a00070b9cba",
          name: "F6",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/25. BR191 F6.mp4",
          checksum: '"97ce0b6a648412c7751853d3c555e8e7-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61cccd6b24aa9a00070b9cdb",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/26. GlovesOn.mp4",
          checksum: '"c407ef0f524c81c98d8348a82cf9297e-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd3624aa9a00070b9cbd",
          name: "R7",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/27. BR191 R7.mp4",
          checksum: '"5bb963203a37292a83838b2e4cced35d-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 7,
            start_in: 17240,
            hits: 144,
            sequence: "00X000X0XXXXX0X0X00XX0X0XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61cccd6924aa9a00070b9cd5",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/28. GlovesOff.mp4",
          checksum: '"e762280c0300bbab072369703b68b52f"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd3624aa9a00070b9cbe",
          name: "F7",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/29. BR191 F7.mp4",
          checksum: '"dea4e4951e46b3612a1b45c63ea4688e-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61cccd6b24aa9a00070b9cdc",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/30. GlovesOn.mp4",
          checksum: '"d31a52569ff221c275ae8668e22273d9-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd3524aa9a00070b9cbc",
          name: "R8",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/31. BR191 R8.mp4",
          checksum: '"9652f1aca87599afbc18f7444f2f25c5-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/round.png",
            name: "ROUND",
            round: 8,
            start_in: 17240,
            hits: 144,
            sequence: "00X000X0XXXXX0X0X00XX0X0XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61cccd6a24aa9a00070b9cd6",
          name: "GlovesOff",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/32. GlovesOff.mp4",
          checksum: '"cf2c16819ce86196ad1f89cc455fcb26"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/glovesoff.png",
            name: "OTHERS",
          },
        },
        {
          id: "61cccd4024aa9a00070b9cbf",
          name: "F8",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/33. BR191 F8.mp4",
          checksum: '"6cb08871a3462a9166ab9026bc59cc17-7"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/functional.png",
            name: "FUNCTIONAL",
          },
        },
        {
          id: "61cccd4c24aa9a00070b9cc2",
          name: "CoolDown191",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/34. CoolDown191.mp4",
          checksum: '"9a0eb9fbcdaec68104f2637f0dcddcb1-17"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/cooldown.png",
            name: "COOL_DOWN",
          },
        },
        {
          id: "61d4882624aa9a00070f1014",
          name: "H4C_191",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/BR191/35. H4C_191.mp4",
          checksum: '"20884c3b54bdb08ae9cc435476720502"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/h4c_191.png",
            name: "OTHERS",
          },
        },
      ],
      cheatsheet: {
        1: ["Slip", "", "Upper", "", "Slip", "", "Upper", ""],
        2: ["Upper", "Hook", "Upper", "Hook", "Low Kick", "", "Hook", ""],
        3: "Round 1 + Round 2",
        4: ["Cross", "Slip", "Slip", "Upper", "Upper", "", "Hook", ""],
        5: ["Jab", "Cross", "Jab", "Cross", "Jab", "", "Middle Kick", ""],
        6: "Round 4 + Round 5",
        7: "Round 1 + Round 2 + Round 4 + Round 5",
        8: "Round 1 + Round 2 + Round 4 + Round 5",
      },
      isDownloaded: false,
    },
  ],
  games: [
    {
      id: "CH5",
      name: "Challenge #CH5",
      published: true,
      from: "Invalid Date",
      to: "Invalid Date",
      vid_url: "",
      vid_tec_url: "",
      img_url: "",
      desc_es: "",
      desc_en: "",
      desc_it: "",
      desc_ru: "",
      calories: 0,
      duration: 0,
      techMoves: "",
      trainer: "",
      cover:
        "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH5/images/FWG5_BR.jpg",
      isActive: false,
      videos: [
        {
          id: "61f028dc24aa9a0007253275",
          name: "WarmUp5",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH5/01. WarmUp5.mp4",
          checksum: '"1168dd60aa46c4c11d7ed61f2e6e4ce3-3"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
            name: "WARM_UP",
          },
        },
        {
          id: "61eff4dc24aa9a000724f5c3",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH5/02. GlovesOn.mp4",
          checksum: '"4894b4bab057c1020d251b175b59bf70"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61eff52c24aa9a000724f5d4",
          name: "R1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH5/03. BR5 R1.mp4",
          checksum: '"c90d5f67796dacbe1771c451c7e1636c-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 1,
            start_in: 17240,
            hits: 128,
            sequence: "XXX000X0X0X0X0X0",
            bpm: 134,
          },
        },
        {
          id: "61eff4f624aa9a000724f5ce",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH5/04. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61eff52e24aa9a000724f5d5",
          name: "R2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH5/05. BR5 R2.mp4",
          checksum: '"6a89466fa988a2378c73f272f2c43e7e-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 2,
            start_in: 17240,
            hits: 128,
            sequence: "X0X0X0X0XXX000X0",
            bpm: 134,
          },
        },
        {
          id: "61eff4f824aa9a000724f5cf",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH5/06. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61eff52a24aa9a000724f5d2",
          name: "R3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH5/07. BR5 R3.mp4",
          checksum: '"f0ad3d2240194d25bfb8ba73b3b3c454-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 3,
            start_in: 17240,
            hits: 128,
            sequence: "XXX000X0X0X0X0X0X0X0X0X0XXX000X0",
            bpm: 134,
          },
        },
      ],
      cheatsheet: {
        1: [
          "Jab",
          "Cross",
          "Middle Kick",
          "",
          "Step",
          "",
          "Cross",
          "",
          "Upper",
          "",
          "Upper",
          "",
          "Hook",
          "Slip",
          "Upper",
          "",
        ],
        2: [
          "Jab",
          "",
          "Middle Kick",
          "",
          "Jab",
          "Rock Back",
          "Cross",
          "",
          "Jab",
          "Cross",
          "Hook",
          "Slip",
          "Weave",
          "",
          "Cross",
          "",
        ],
        3: "Combo 1 + Combo 2",
      },
      isDownloaded: false,
    },
    {
      id: "CH6",
      name: "Challenge #CH6",
      published: true,
      from: "Invalid Date",
      to: "Invalid Date",
      vid_url: "",
      vid_tec_url: "",
      img_url: "",
      desc_es: "",
      desc_en: "",
      desc_it: "",
      desc_ru: "",
      calories: 0,
      duration: 0,
      techMoves: "",
      trainer: "",
      cover:
        "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH6/images/FWG6_BR.jpg",
      isActive: false,
      videos: [
        {
          id: "61f0290e24aa9a00072536f6",
          name: "WarmUp6",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH6/01. WarmUp6.mp4",
          checksum: '"b758181e1fcf0dbab1fd3555d6e4d264-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
            name: "WARM_UP",
          },
        },
        {
          id: "61eff57524aa9a000724f5d9",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH6/02. GlovesOn.mp4",
          checksum: '"4894b4bab057c1020d251b175b59bf70"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61eff59e24aa9a000724f5e5",
          name: "R1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH6/03. BR6 R1.mp4",
          checksum: '"994b4cdadfa5c68cda5f0a5a6c9456fd-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 1,
            start_in: 17276,
            hits: 150,
            sequence: "XXX0X0XXX0X0XXX0",
            bpm: 134,
          },
        },
        {
          id: "61eff57824aa9a000724f5da",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH6/04. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61eff59d24aa9a000724f5e4",
          name: "R2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH6/05. BR6 R2.mp4",
          checksum: '"e5d076d275d040c7573d1b58c877e754-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 2,
            start_in: 17276,
            hits: 160,
            sequence: "00XXX000XXX0X0X0",
            bpm: 134,
          },
        },
        {
          id: "61eff57924aa9a000724f5db",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH6/06. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61eff59f24aa9a000724f5e6",
          name: "R3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH6/07. BR6 R3.mp4",
          checksum: '"571cace7709ba030d4eff293f5dccb3d-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 3,
            start_in: 17240,
            hits: 155,
            sequence: "XXX0X0XXX0X0XXX000XXX000XXX0X0X0",
            bpm: 134,
          },
        },
      ],
      cheatsheet: {
        1: [
          "Jab",
          "Jab",
          "Cross",
          "",
          "Middle Kick",
          "",
          "Jab",
          "Cross",
          "Hook",
          "",
          "Hook",
          "",
          "Upper",
          "Upper",
          "Jab",
          "",
        ],
        2: [
          "Twist",
          "",
          "Cross",
          "Upper",
          "Cross",
          "",
          "Step",
          "",
          "Jab",
          "Cross",
          "Jab",
          "Slip",
          "Upper",
          "",
          "Middle Kick",
          "",
        ],
        3: "Combo 1 + Combo 2",
      },
      isDownloaded: false,
    },
    {
      id: "CH4",
      name: "Challenge #CH4",
      published: true,
      from: "Invalid Date",
      to: "Invalid Date",
      vid_url: "",
      vid_tec_url: "",
      img_url: "",
      desc_es: "",
      desc_en: "",
      desc_it: "",
      desc_ru: "",
      calories: 0,
      duration: 0,
      techMoves: "",
      trainer: "",
      cover:
        "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH4/images/FWG4_BR.jpg",
      isActive: false,
      videos: [
        {
          id: "61f028f124aa9a00072533c9",
          name: "WarmUp4",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH4/01. WarmUp4.mp4",
          checksum: '"05517706a3d0aba75de33a4755d1a50b-3"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
            name: "WARM_UP",
          },
        },
        {
          id: "61eff47c24aa9a000724f5b5",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH4/02. GlovesOn.mp4",
          checksum: '"4894b4bab057c1020d251b175b59bf70"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61eff4b024aa9a000724f5bd",
          name: "R1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH4/03. BR4 R1.mp4",
          checksum: '"6a7e6065b9e47261c28522bca0f8b4c9-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 1,
            start_in: 17240,
            hits: 160,
            sequence: "XXX0X0XXX000X0X0",
            bpm: 134,
          },
        },
        {
          id: "61eff48024aa9a000724f5b7",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH4/04. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61eff4af24aa9a000724f5bc",
          name: "R2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH4/05. BR4 R2.mp4",
          checksum: '"ed179acc8c406f1630f3b3774a7b336a-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 2,
            start_in: 17240,
            hits: 96,
            sequence: "XX00X0X0XXX0X0X0",
            bpm: 134,
          },
        },
        {
          id: "61eff48024aa9a000724f5b6",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH4/06. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61eff4ad24aa9a000724f5bb",
          name: "R3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH4/07. BR4 R3.mp4",
          checksum: '"f42b102004fb6eac23c62b603436ce62-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 3,
            start_in: 17240,
            hits: 128,
            sequence: "XXX0X0XXX000X0X0XX00X0X0XXX0X0X0",
            bpm: 134,
          },
        },
      ],
      cheatsheet: {
        1: [
          "Jab",
          "Cross",
          "Hook",
          "",
          "Hook",
          "",
          "Low Jab",
          "Low Cross",
          "Hook",
          "",
          "Step",
          "Step",
          "Jab",
          "",
          "Low Kick",
          "",
        ],
        2: [
          "Jab",
          "Cross",
          "Slip",
          "Slip",
          "Upper",
          "",
          "Middle Kick",
          "",
          "Upper",
          "Hook",
          "Hook",
          "",
          "Low Kick",
          "",
          "Cross",
          "",
        ],
        3: "Combo 1 + Combo 2",
      },
      isDownloaded: false,
    },
    {
      id: "CH3",
      name: "Challenge #CH3",
      published: true,
      from: "Invalid Date",
      to: "Invalid Date",
      vid_url: "",
      vid_tec_url: "",
      img_url: "",
      desc_es: "",
      desc_en: "",
      desc_it: "",
      desc_ru: "",
      calories: 0,
      duration: 0,
      techMoves: "",
      trainer: "",
      cover:
        "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH3/images/FWG3_BR.jpg",
      isActive: false,
      videos: [
        {
          id: "61f0289524aa9a0007253231",
          name: "WarmUp3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH3/01. WarmUp3.mp4",
          checksum: '"208b50133b5daba69823eed8b86f7f9e-3"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
            name: "WARM_UP",
          },
        },
        {
          id: "61efef6f24aa9a000724f19b",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH3/02. GlovesOn.mp4",
          checksum: '"4894b4bab057c1020d251b175b59bf70"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61efef5424aa9a000724f198",
          name: "R1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH3/03. BR3 R1.mp4",
          checksum: '"42d65c71c562646b5995770ac5744443-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 1,
            start_in: 17268,
            hits: 160,
            sequence: "XXX0X0X000X000X0",
            bpm: 134,
          },
        },
        {
          id: "61efef7024aa9a000724f19c",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH3/04. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61efef5b24aa9a000724f199",
          name: "R2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH3/05. BR3 R2.mp4",
          checksum: '"5963d13c7903b34b526499bd0e3f1078-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 2,
            start_in: 17268,
            hits: 64,
            sequence: "XXX0X0X0X0X0XXXX",
            bpm: 134,
          },
        },
        {
          id: "61efef7124aa9a000724f19d",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH3/06. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61efef5d24aa9a000724f19a",
          name: "R3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH3/07. BR3 R3.mp4",
          checksum: '"ade06bd77a213628fefbc2f033d35190-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 3,
            start_in: 17268,
            hits: 112,
            sequence: "XXX0X0X000X000X0XXX0X0X0X0X0XXXX",
            bpm: 134,
          },
        },
      ],
      cheatsheet: {
        1: [
          "Jab",
          "Cross",
          "Jab",
          "",
          "High Upper",
          "",
          "Jab",
          "",
          "",
          "",
          "Back Kick",
          "",
          "",
          "",
          "Middle Kick",
          "",
        ],
        2: [
          "Jab",
          "High Upper",
          "Cross",
          "",
          "Middle Kick",
          "",
          "Cross",
          "",
          "Low Kick",
          "",
          "Hook",
          "",
          "Upper",
          "Hook",
          "Upper",
          "Hook",
        ],
        3: "Combo 1 + Combo 2",
      },
      isDownloaded: false,
    },
    {
      id: "CH1",
      name: "Challenge #CH1",
      published: true,
      from: "Invalid Date",
      to: "Invalid Date",
      vid_url: "",
      vid_tec_url: "",
      img_url: "",
      desc_es: "",
      desc_en: "",
      desc_it: "",
      desc_ru: "",
      calories: 0,
      duration: 0,
      techMoves: "",
      trainer: "",
      cover:
        "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH1/images/FWG1_BR.jpg",
      isActive: false,
      videos: [
        {
          id: "61f0286024aa9a000725322d",
          name: "WarmUp1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH1/01. WarmUp1.mp4",
          checksum: '"8a4fc76e6a88898912aacb4e5fd8c596-2"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
            name: "WARM_UP",
          },
        },
        {
          id: "61efcc4b24aa9a000724d2dd",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH1/02. GlovesOn.mp4",
          checksum: '"4894b4bab057c1020d251b175b59bf70"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61efcc3a24aa9a000724d2d5",
          name: "R1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH1/03. BR1 R1.mp4",
          checksum: '"34bda0c8fca8e0d6e6b6519570559514-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 1,
            start_in: 17240,
            hits: 160,
            sequence: "X0X0X0XX00X0XXXX",
            bpm: 134,
          },
        },
        {
          id: "61efcc4d24aa9a000724d2df",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH1/04. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61efcc4024aa9a000724d2db",
          name: "R2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH1/05. BR1 R2.mp4",
          checksum: '"14a302ccc61cad3d902e69e4b5ff0d83-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 2,
            start_in: 17240,
            hits: 160,
            sequence: "00X0XXXXXX00X0X0",
            bpm: 134,
          },
        },
        {
          id: "61efcc4d24aa9a000724d2de",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH1/06. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61efcc3f24aa9a000724d2da",
          name: "R3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH1/07. BR1 R3.mp4",
          checksum: '"34f5c764f641f9eea14f05b375fe1111-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 3,
            start_in: 17240,
            hits: 160,
            sequence: "X0X0X0XX00X0XXXX00X0XXXXXX00X0X0",
            bpm: 134,
          },
        },
      ],
      cheatsheet: {
        1: [
          "Jab",
          "",
          "Low Kick",
          "",
          "Middle Kick",
          "",
          "Cross",
          "Jab",
          "Weave",
          "",
          "Upper",
          "",
          "Elbow",
          "Elbow",
          "Upper",
          "Hook",
        ],
        2: [
          "Weave",
          "",
          "Middle Kick",
          "",
          "Jab",
          "Cross",
          "Hook",
          "Upper",
          "Jab",
          "Cross",
          "Check",
          "",
          "Low Kick",
          "",
          "Cross",
          "",
        ],
        3: "Combo 1 + Combo 2",
      },
      isDownloaded: true,
    },
    {
      id: "CH2",
      name: "Challenge #CH2",
      published: true,
      from: "Invalid Date",
      to: "Invalid Date",
      vid_url: "",
      vid_tec_url: "",
      img_url: "",
      desc_es: "",
      desc_en: "",
      desc_it: "",
      desc_ru: "",
      calories: 0,
      duration: 0,
      techMoves: "",
      trainer: "",
      cover:
        "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH2/images/FWG2_BR.jpg",
      isActive: false,
      videos: [
        {
          id: "61f0288224aa9a0007253230",
          name: "WarmUp2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH2/01. WarmUp2.mp4",
          checksum: '"deee4770c3f1b1a7f050500b1fb86695-3"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/warmup.png",
            name: "WARM_UP",
          },
        },
        {
          id: "61efd07024aa9a000724e37f",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH2/02. GlovesOn.mp4",
          checksum: '"4894b4bab057c1020d251b175b59bf70"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61efd0cd24aa9a000724e39c",
          name: "R1",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH2/03. BR2 R1.mp4",
          checksum: '"4f67e33e62669f4c03057c6aad24db3b-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 1,
            start_in: 17240,
            hits: 160,
            sequence: "X0X0XXX0XXXX0XX0",
            bpm: 134,
          },
        },
        {
          id: "61efd07224aa9a000724e380",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH2/04. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61efd0d424aa9a000724e3a0",
          name: "R2",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH2/05. BR2 R2.mp4",
          checksum: '"c93a511c02acadbecd25988b93a72999-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 2,
            start_in: 17240,
            hits: 192,
            sequence: "X000X0X0XXXXX0X0",
            bpm: 134,
          },
        },
        {
          id: "61efd07224aa9a000724e381",
          name: "GlovesOn",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH2/06. GlovesOn.mp4",
          checksum: '"b4e0a22b0212a97d67c8d09f50b48fec"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/gloveson.png",
            name: "OTHERS",
          },
        },
        {
          id: "61efd0ce24aa9a000724e39d",
          name: "R3",
          url:
            "https://brooklyn-videos.s3-eu-west-1.amazonaws.com/BROOKLYN/CH2/07. BR2 R3.mp4",
          checksum: '"5236c9f64b9df2068bcf7f9571db9ff3-8"',
          type: {
            url_image:
              "https://brooklyn-videos.s3.eu-west-1.amazonaws.com/images/combo.png",
            name: "COMBO",
            round: 3,
            start_in: 17240,
            hits: 176,
            sequence: "X0X0XXX0XXXX0XX0X000X0X0XXXXX0X0",
            bpm: 134,
          },
        },
      ],
      cheatsheet: {
        1: [
          "Middle Kick",
          "",
          "Middle Kick",
          "",
          "Upper",
          "Upper",
          "Cross",
          "",
          "Jab",
          "Jab",
          "Cross",
          "Hook",
          "Slip",
          "Upper",
          "Cross",
          "",
        ],
        2: [
          "Upper",
          "",
          "Switch",
          "",
          "Low Kick",
          "",
          "Hook",
          "",
          "Jab",
          "Low Cross",
          "Low Jab",
          "Cross",
          "Upper",
          "",
          "Upper",
          "",
        ],
        3: "Combo 1 + Combo 2",
      },
      isDownloaded: true,
    },
  ],
  tournament: [
    {
      id: "61eaccf60274390008a5dd2e",
      name: "TOURNAMENT",
      published: true,
      from: "2022-01-16T23:00:00.000Z",
      to: "2022-01-29T23:00:00.000Z",
      vid_url: "https://www.youtube.com/embed/CO2MG9wWIxg",
      vid_tec_url: "https://www.youtube.com/embed/FxBqvGXItZs",
      img_url:
        "https://www.brooklynfitboxing.com/world-games/wp-content/uploads/2022/01/cheat_seat_-_192.jpeg",
      desc_es:
        "¿Echabais de menos los challenge cañeros?\n¿Preparados para darlos todo en el saco?\n¡LLEGA EL #BFChallenge192 para que demuestres todo lo que sabes!",
      desc_en:
        "Did you miss the hardcore challenges?\nReady to give your all at the punching bag?\nThe #BFChallenge192 has arrived to help you show off everything you know!",
      desc_it: "",
      desc_ru: "",
      calories: 824,
      duration: 47,
      techMoves:
        "STEP\n· Iniciar con el pie más cercano a la dirección del paso\n· Deslizar, nunca saltar\n· Ambos pies recorren la misma distancia\n· Los brazos se mantienen en la posición de guardia\n· Mantener la separación de pies al final del movimiento\n· Mantener el tronco recto\n· Mantener la barbilla protegida entre los guantes",
      trainer: "Jorge Roglá",
      cover: "",
      isActive: true,
      videos: [],
      cheatsheet: {},
      isDownloaded: true,
    },
  ],
};

const tournamentCombats = [
  {
    team1Group: null,
    name: null,
    team2Group: null,
    phase: "620e9c4b0274390008a5e144",
    participants: {
      team1: [
        {
          id: "61b35ccc02743900080d73a6",
          name: "José",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0b4902743900080d7344.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61b35e5f02743900080d73a7",
          name: "María",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0efd02743900080d7349.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61b35ede02743900080d73a8",
          name: "Tyrus",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61e92f2d0274390008a5dd01.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61b35f7002743900080d73a9",
          name: "Archie",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0edc02743900080d7348.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61b35f7002743900080d73a9",
          name: "Karlen",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61a50f0b027439000956e963.png",
          bag: 0,
          pow: 800,
        }
      ],
      team2: [
        {
          id: "61a50f0b027439000956e963",
          name: "gley",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61a50f0b027439000956e963.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61aa0b4902743900080d7344",
          name: "Marc",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0b4902743900080d7344.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61aa0edc02743900080d7348",
          name: "Petter",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0edc02743900080d7348.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61aa0efd02743900080d7349",
          name: "Ana",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0efd02743900080d7349.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61e92f2d0274390008a5dd01",
          name: "Rudy",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61e92f2d0274390008a5dd01.png",
          bag: 0,
          pow: 800,
        },
      ],
    },
    score1: 8,
    id: "620e9c4b0274390008a5e14a",
    team1Pos: null,
    lastCombo: 0,
    score2: 7,
    status: "P",
    time: 1515,
    game: null,
    group: "620e9c4b0274390008a5e146",
    team2Pos: null,
    team1: {
      name: "Monsters",
      updatedAt: "2022-02-11T16:44:55.853Z",
      code: "2rhcgo4m",
      logo:  "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/61b5e78202743900080d7486.png",
      id: "61b622f602743900080d7488",
      order: 5,
      createdAt: "2021-12-12T16:27:34.110Z",
      registrationDate: "2022-02-11T16:44:55.849Z",
      motto: "A por ellos criaturas!!",
    },
    team2: {
      name: "Gran coquivacoa",
      updatedAt: "2022-02-14T10:14:55.855Z",
      code: "4wy2sfa6f",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/61aa348502743900080d7355.jpg",
      id: "61aa348502743900080d7355",
      order: 8,
      createdAt: "2021-12-03T15:15:17.551Z",
      registrationDate: "2022-02-14T10:14:55.852Z",
      motto: "gfggfgf111",
    },
  },
  {
    team1Group: null,
    name: null,
    team2Group: null,
    phase: "620e9c4b0274390008a5e144",
    participants: {
      team1: [
        {
          id: "61b35ccc02743900080d73a6",
          name: "Aureliano",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61b35e5f02743900080d73a7",
          name: "Aureliano",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61b35ede02743900080d73a8",
          name: "Aureliano",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61b35f7002743900080d73a9",
          name: "Aureliano",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61b35f7002743900080d73a9",
          name: "Aureliano",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
          bag: 0,
          pow: 800,
        }
      ],
      team2: [
        {
          id: "61a50f0b027439000956e963",
          name: "gley",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61a50f0b027439000956e963.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61aa0b4902743900080d7344",
          name: "Marc",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0b4902743900080d7344.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61aa0edc02743900080d7348",
          name: "Petter",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0edc02743900080d7348.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61aa0efd02743900080d7349",
          name: "Ana",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0efd02743900080d7349.png",
          bag: 0,
          pow: 800,
        },
        {
          id: "61e92f2d0274390008a5dd01",
          name: "Rudy",
          logo:
            "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61e92f2d0274390008a5dd01.png",
          bag: 0,
          pow: 800,
        },
      ],
    },
    score1: 8,
    id: "620e9c4b0274390008a5e14a",
    team1Pos: null,
    lastCombo: 0,
    score2: 7,
    status: "P",
    time: 1515,
    game: null,
    group: "620e9c4b0274390008a5e146",
    team2Pos: null,
    team1: {
      name: "Monsters",
      updatedAt: "2022-02-11T16:44:55.853Z",
      code: "2rhcgo4m",
      logo: null,
      id: "61b622f602743900080d7488",
      order: 5,
      createdAt: "2021-12-12T16:27:34.110Z",
      registrationDate: "2022-02-11T16:44:55.849Z",
      motto: "A por ellos criaturas!!",
    },
    team2: {
      name: "Gran coquivacoa",
      updatedAt: "2022-02-14T10:14:55.855Z",
      code: "4wy2sfa6f",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/61aa348502743900080d7355.jpg",
      id: "61aa348502743900080d7355",
      order: 8,
      createdAt: "2021-12-03T15:15:17.551Z",
      registrationDate: "2022-02-14T10:14:55.852Z",
      motto: "gfggfgf111",
    },
  },
];
const participants = {
  team1: [
    {
      id: "61b35ccc02743900080d73a6",
      name: "Aureliano",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
      bag: 1,
      pow: 800,
    },
    {
      id: "61b35ccc02743900080d73a6",
      name: "Aureliano",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
      bag: 2,
      pow: 800,
    },
    {
      id: "61b35ccc02743900080d73a6",
      name: "Aureliano",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
      bag: 3,
      pow: 800,
    },
    {
      id: "61b35ccc02743900080d73a6",
      name: "Aureliano",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
      bag: 4,
      pow: 800,
    },
  ],
  team2: [
    {
      id: "61a50f0b027439000956e963",
      name: "gley",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61a50f0b027439000956e963.png",
      bag: 10,
      pow: 800,
    },
    {
      id: "61aa0b4902743900080d7344",
      name: "Marc",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0b4902743900080d7344.png",
      bag: 11,
      pow: 800,
    },
    {
      id: "61aa0edc02743900080d7348",
      name: "Petter",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0edc02743900080d7348.png",
      bag: 12,
      pow: 800,
    },
    {
      id: "61aa0efd02743900080d7349",
      name: "Ana",
      logo:
        "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0efd02743900080d7349.png",
      bag: 13,
      pow: 800,
    }
  ],
};
const combats = [
  {
      arena: 1,
      team1Group: null,
      name: null,
      team2Group: null,
      phase: "620e9c4b0274390008a5e144",
      participants: {
        team1: [
          {
            id: "61b35ccc02743900080d73a6",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35e5f02743900080d73a7",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35ede02743900080d73a8",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35f7002743900080d73a9",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35f7002743900080d73a9",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          }
        ],
        team2: [
          {
            id: "61a50f0b027439000956e963",
            name: "gley",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61a50f0b027439000956e963.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0b4902743900080d7344",
            name: "Marc",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0b4902743900080d7344.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0edc02743900080d7348",
            name: "Petter",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0edc02743900080d7348.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0efd02743900080d7349",
            name: "Ana",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0efd02743900080d7349.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61e92f2d0274390008a5dd01",
            name: "Rudy",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61e92f2d0274390008a5dd01.png",
            bag: 0,
            pow: 800,
          },
        ],
      },
      score1: 8,
      id: "620e9c4b0274390008a5e14a",
      team1Pos: null,
      lastCombo: 0,
      score2: 7,
      status: "P",
      time: 1515,
      game: null,
      group: "620e9c4b0274390008a5e146",
      team2Pos: null,
      team1: {
        name: "Monsters",
        updatedAt: "2022-02-11T16:44:55.853Z",
        code: "2rhcgo4m",
        logo:  "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
        id: "61b622f602743900080d7488",
        order: 5,
        createdAt: "2021-12-12T16:27:34.110Z",
        registrationDate: "2022-02-11T16:44:55.849Z",
        motto: "A por ellos criaturas!!",
      },
      team2: {
        name: "Gran coquivacoa",
        updatedAt: "2022-02-14T10:14:55.855Z",
        code: "4wy2sfa6f",
        logo:
          "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/61aa348502743900080d7355.jpg",
        id: "61aa348502743900080d7355",
        order: 8,
        createdAt: "2021-12-03T15:15:17.551Z",
        registrationDate: "2022-02-14T10:14:55.852Z",
        motto: "gfggfgf111",
      },

  },
  {
      arena: 2,
      team1Group: null,
      name: null,
      team2Group: null,
      phase: "620e9c4b0274390008a5e144",
      participants: {
        team1: [
          {
            id: "61b35ccc02743900080d73a6",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35e5f02743900080d73a7",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35ede02743900080d73a8",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35f7002743900080d73a9",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35f7002743900080d73a9",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          }
        ],
        team2: [
          {
            id: "61a50f0b027439000956e963",
            name: "gley",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61a50f0b027439000956e963.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0b4902743900080d7344",
            name: "Marc",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0b4902743900080d7344.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0edc02743900080d7348",
            name: "Petter",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0edc02743900080d7348.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0efd02743900080d7349",
            name: "Ana",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0efd02743900080d7349.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61e92f2d0274390008a5dd01",
            name: "Rudy",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61e92f2d0274390008a5dd01.png",
            bag: 0,
            pow: 800,
          },
        ],
      },
      score1: 8,
      id: "620e9c4b0274390008a5e14a",
      team1Pos: null,
      lastCombo: 0,
      score2: 7,
      status: "P",
      time: 1515,
      game: null,
      group: "620e9c4b0274390008a5e146",
      team2Pos: null,
      team1: {
        name: "Monsters",
        updatedAt: "2022-02-11T16:44:55.853Z",
        code: "2rhcgo4m",
        logo: null,
        id: "61b622f602743900080d7488",
        order: 5,
        createdAt: "2021-12-12T16:27:34.110Z",
        registrationDate: "2022-02-11T16:44:55.849Z",
        motto: "A por ellos criaturas!!",
      },
      team2: {
        name: "Gran coquivacoa",
        updatedAt: "2022-02-14T10:14:55.855Z",
        code: "4wy2sfa6f",
        logo:
          "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/61aa348502743900080d7355.jpg",
        id: "61aa348502743900080d7355",
        order: 8,
        createdAt: "2021-12-03T15:15:17.551Z",
        registrationDate: "2022-02-14T10:14:55.852Z",
        motto: "gfggfgf111",
      },
  },
  {
      arena: 3,
      team1Group: null,
      name: null,
      team2Group: null,
      phase: "620e9c4b0274390008a5e144",
      participants: {
        team1: [
          {
            id: "61b35ccc02743900080d73a6",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35e5f02743900080d73a7",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35ede02743900080d73a8",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35f7002743900080d73a9",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35f7002743900080d73a9",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          }
        ],
        team2: [
          {
            id: "61a50f0b027439000956e963",
            name: "gley",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61a50f0b027439000956e963.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0b4902743900080d7344",
            name: "Marc",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0b4902743900080d7344.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0edc02743900080d7348",
            name: "Petter",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0edc02743900080d7348.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0efd02743900080d7349",
            name: "Ana",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0efd02743900080d7349.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61e92f2d0274390008a5dd01",
            name: "Rudy",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61e92f2d0274390008a5dd01.png",
            bag: 0,
            pow: 800,
          },
        ],
      },
      score1: 8,
      id: "620e9c4b0274390008a5e14a",
      team1Pos: null,
      lastCombo: 0,
      score2: 7,
      status: "P",
      time: 1515,
      game: null,
      group: "620e9c4b0274390008a5e146",
      team2Pos: null,
      team1: {
        name: "Monsters",
        updatedAt: "2022-02-11T16:44:55.853Z",
        code: "2rhcgo4m",
        logo: null,
        id: "61b622f602743900080d7488",
        order: 5,
        createdAt: "2021-12-12T16:27:34.110Z",
        registrationDate: "2022-02-11T16:44:55.849Z",
        motto: "A por ellos criaturas!!",
      },
      team2: {
        name: "Gran coquivacoa",
        updatedAt: "2022-02-14T10:14:55.855Z",
        code: "4wy2sfa6f",
        logo:
          "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/61aa348502743900080d7355.jpg",
        id: "61aa348502743900080d7355",
        order: 8,
        createdAt: "2021-12-03T15:15:17.551Z",
        registrationDate: "2022-02-14T10:14:55.852Z",
        motto: "gfggfgf111",
      },
    },
  {
      arena: 4,
      team1Group: null,
      name: null,
      team2Group: null,
      phase: "620e9c4b0274390008a5e144",
      participants: {
        team1: [
          {
            id: "61b35ccc02743900080d73a6",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35e5f02743900080d73a7",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35ede02743900080d73a8",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35f7002743900080d73a9",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61b35f7002743900080d73a9",
            name: "Aureliano",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
            bag: 0,
            pow: 800,
          }
        ],
        team2: [
          {
            id: "61a50f0b027439000956e963",
            name: "gley",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61a50f0b027439000956e963.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0b4902743900080d7344",
            name: "Marc",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0b4902743900080d7344.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0edc02743900080d7348",
            name: "Petter",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0edc02743900080d7348.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61aa0efd02743900080d7349",
            name: "Ana",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61aa0efd02743900080d7349.png",
            bag: 0,
            pow: 800,
          },
          {
            id: "61e92f2d0274390008a5dd01",
            name: "Rudy",
            logo:
              "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61e92f2d0274390008a5dd01.png",
            bag: 0,
            pow: 800,
          },
        ],
      },
      score1: 8,
      id: "620e9c4b0274390008a5e14a",
      team1Pos: null,
      lastCombo: 0,
      score2: 7,
      status: "P",
      time: 1515,
      game: null,
      group: "620e9c4b0274390008a5e146",
      team2Pos: null,
      team1: {
        name: "Monsters",
        updatedAt: "2022-02-11T16:44:55.853Z",
        code: "2rhcgo4m",
        logo:  "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/61b35ccc02743900080d73a6.png",
        id: "61b622f602743900080d7488",
        order: 5,
        createdAt: "2021-12-12T16:27:34.110Z",
        registrationDate: "2022-02-11T16:44:55.849Z",
        motto: "A por ellos criaturas!!",
      },
      team2: {
        name: "Gran coquivacoa",
        updatedAt: "2022-02-14T10:14:55.855Z",
        code: "4wy2sfa6f",
        logo:
          "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/61aa348502743900080d7355.jpg",
        id: "61aa348502743900080d7355",
        order: 8,
        createdAt: "2021-12-03T15:15:17.551Z",
        registrationDate: "2022-02-14T10:14:55.852Z",
        motto: "gfggfgf111",
      },
    }
];

var express = require("express");
var router = express.Router();

router.get("/challenges", function (req, res, next) {
  res.send(data);
});
/* router.get("/:description", (req, res) => {
  const description = String(req.params.description);
  const challenge = challenges.find(
    (challenge) => challenge.description === description
  );
  res.send(challenge);
});
 */
router.get("/combats", function (req, res, next) {
  res.send(tournamentCombats);
});
router.get("/participants", function (req, res, next) {
  res.send(participants);
});
router.get("/combatsFinal", function (req, res, next) {
  res.send(combats);
});
module.exports = router;
