const express = require('express');
const { exec, execSync } = require('child_process');
const fs = require('fs');
const unzip = require('unzipper');
const Rsync = require('rsync');
const util = require('util');
const fetch = require('node-fetch');
const { getCenter } = require('../services/center');
const execPromise = util.promisify(exec);
const router = express.Router();
const backupFolderBackend = `/tmp/backup/pb-backend/`;
const backupFolderFrontend = `/tmp/backup/pb-frontend/`;

function rsyncApp(folderSou, folderDest) {
  return new Promise((res, rej) => {
    var rsync = new Rsync()
      .flags('avzP')
      .set('delete')
      .set('recursive')
      .set('progress')
      .source(folderSou)
      .destination(folderDest)
    try {
      rsync.execute(function (error, code, cmd) {
        res(cmd);
      },
        (stdout) => { },
        (stderr) => { }
      )
    } catch (error) {
      console.error(error)
      rej(error);
    }
  });
}

const downloadFile = (async (fileUrl, filePath) => {
  const res = await fetch(fileUrl);
  const fileStream = fs.createWriteStream(filePath);
  return new Promise((resolve, reject) => {
    res.body.pipe(fileStream);
    res.body.on("error", reject);
    fileStream.on("finish", resolve);
  });
});

async function createFolder(folder) {
  console.log(`Creating folder:${folder}`)
  if (fs.existsSync(folder)) {
    execSync(`rm -rf ${folder} `);
  }
  fs.mkdirSync(folder);
  console.log('Created folder: ' + folder)
}

async function updatePermissions() {
  const { stderr1, stdout1 } = await execPromise(`echo ${process.env.db_password} | sudo -S chmod 777 /tmp/ -R `)
  const { stderr2, stdout2 } = await execPromise(`echo ${process.env.db_password} | sudo -S chmod 777 /var/www/playbrooklyn/ -R `)
  const { stderr3, stdout3 } = await execPromise(`echo ${process.env.db_password} | sudo -S chmod 777 /var/www/playserver/ -R `)
  const { stderr4, stdout4 } = await execPromise(`echo ${process.env.db_password} | sudo -S chmod 777 /home/minipc/playbrooklyn-server/ -R `)
  console.log(stderr1)
  console.log(stderr2)
  console.log(stderr3)
  console.log(stderr4)
}

async function backupCurrentVersion() {
  try {
    createFolder(backupFolderBackend);
    console.log('Created folder: ' + backupFolderBackend)
    await rsyncApp(`/home/minipc/playbrooklyn-server`, backupFolderBackend)
    await rsyncApp(`/home/minipc/playbrooklyn-server`, backupFolderFrontend)
  } catch (err) {
    console.error('Error trying to backup current playbrooklyn version in minipc:' + err.message);
    throw new Error('Error trying to backup current playbrooklyn version in minipc:' + err.message);
  }
  console.log(`Backup executed successfully`)
}

async function rollback() {
  try {
    await rsyncApp(backupFolderBackend, `/home/minipc/playbrooklyn-server`)
    await rsyncApp(backupFolderFrontend, `/home/minipc/playbrooklyn-server`)

  } catch (err) {
    console.error('Error trying to restore current playbrooklyn version in minipc:' + err.message);
    throw new Error('Error trying to restore current playbrooklyn version in minipc:' + err.message);
  }
  console.log(`Rollback executed`)
}
async function unzipFiles (origin, destination) {
 return new Promise((resolve, rej) => {
  fs.createReadStream(origin)
    .pipe(unzip.Extract({ path: destination }))
    .on('finish', function () {
      console.log(`${origin} unzipped succesfully in: ${destination}`);
      resolve(true)
    }).on('error', (err) => {
      console.error(`error unzipping ${origin}`, err)
      rej(err)
    });
});
}
async function upgradeApplication(req, res, next) {
  const center = await getCenter();
  const deployPathFront = "/var/www/playbrooklyn/";
  const deployPathBackend = process.env.NODE_ENV === 'production_f3' ? '/home/minipc/f3-server/' : '/home/minipc/playbrooklyn-server/'

  let backendName = '';
  let frontendName = '';
  if (process.env.NODE_ENV === 'production_f3') {
    backendName = 'f3-backend'
    frontendName = 'f3-frontend'
  } else {
    backendName = 'pb-backend'
    frontendName = 'pb-frontend'
  }
  const zipBack = center.git_branch === 'test' ? `${backendName}_test.zip` : `${backendName}.zip`
  const zipFront = center.git_branch === 'test' ? `${frontendName}_test.zip` : `${frontendName}.zip`

  const downloadFileBack = `/tmp/${zipBack}`
  const downloadFileFront  = `/tmp/${zipFront}`
  try {
    try {
      await updatePermissions()
      await backupCurrentVersion()
    } catch (error) {
      console.error(error)
      res.send({ status: false, message: error.message })
      return
    }
    console.log(`Downloading ${zipBack}`);
    await downloadFile(`${process.env.url_server}${zipBack}`, downloadFileBack );
    console.log(`Downloading ${zipFront}`);
    await downloadFile(`${process.env.url_server}${zipFront}`, downloadFileFront );

    console.log('Files downloaded');
    await createFolder(`/tmp/${backendName}`);
    await createFolder(`/tmp/${frontendName}`);
    console.log('Folders created');

    await unzipFiles(downloadFileBack, `/tmp/${backendName}`)
    await unzipFiles(downloadFileFront, `/tmp/${frontendName}`)
    await rsyncApp(`/tmp/${backendName}/`, deployPathBackend)
    await rsyncApp(`/tmp/${frontendName}/`, deployPathFront)

  
    console.log('Updated both backend and frontend');
    res.status(200).send({ status: true, message: 'application updated' })
    await execPromise('cd /home/minipc/playbrooklyn-server/ && npm install')
    await execPromise('pm2 reload /home/minipc/playbrooklyn-server/ecosystem.config.js --env production');
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: false, message: 'Error al actualizar: ' + error })
    rollback()
  }
}
///////////////////////////////////////////////////////////////////////////
router.get('/', function (req, res, next) {
  res.send('PB Local');
});

router.get('/clean_video_cache/:challenge/', function (req, res, next) {
  const challenge = req.params.challenge;
  res.send({ status: true, message: "Ok, challenge:" + challenge + " removed succesfully" });
});

router.get('/download_challenge/:challenge/', function (req, res, next) {
  const challenge = req.params.challenge;
  res.send({ status: true, message: "Ok, challenge:" + challenge + " is going to be downloaded", data: challenge });
});

router.get('/upgrade_application', upgradeApplication);

module.exports = router;
