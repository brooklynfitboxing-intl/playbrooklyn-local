const axios = require('axios');
const router = require('express').Router();

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
async function getPlayList(req, res) {
}

router.get('/playlist/', getPlayList)
module.exports =  router