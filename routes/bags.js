const express = require('express');
const router = express.Router();
const axios = require('axios');
const { listControllers, assignBagNumber, rebootController } = require('../services/controllers');
const { setBagColor, turnOnBag, turnOffBag, getBags } = require('../services/bags');


router.get('/reboot', async function (req, res, next) { 
  const { bag_number } = req.query
  const bag = (await getBags() || [] ).filter(it => it.bag_number == parseInt(bag_number))[0]
  if(bag){ 
    try {
      await rebootController({delay:2, mac: bag.mac})
    } catch(err){
      console.error(`Error trying to connect to bag:${bag_number}, is not connected or broken `)
      res.status(500).send({ message: `Error trying to connect to bag:${bag_number}, is not connected or broken `})
      return
    }
  } else {
    res.status(404).send({ message: `Bag not found, not connected or not Assigned`})
    return
  }
  res.send({status: true, message:`Restarting bag: ${bag_number} `})
})

router.get('/set_color/:bag_number/:color', async function (req, res, next) {
  const { bag_number, color } = req.params;
  try {
    await setBagColor(bag_number, color)
    res.send({ status: true, message: "Color changed OK" });
  } catch (ex) {
    console.error(ex);
    const status = ex.status || 401
    res.status(status).send({ status: false, message: ex.message });
  }
});

router.get('/get_colors/', async function (req, res, next) {
  const colorConfigFile = require('../public/resources/colors_config')
  res.send(colorConfigFile);
});


router.get('/turn_on/:bag_number', async function (req, res, next) {
  const { bag_number } = req.params;
  try {
    await turnOnBag(bag_number)
    res.send({ status: true, message: `Bag ${bag_number} turned on ` });
  } catch (ex) {
    console.error(ex);
    console.error("Could not connect to bag:" + bag_number + " on host:" + bag.ip, bag);
    res.status(401).send({ status: false, message: "Could not connect to bag:" + bag_number + " on host:" + bag.ip });
  }
});


router.get('/turn_off/:bag_number', async function (req, res, next) {
  const { bag_number } = req.params;
  try {
    await turnOffBag(bag_number)
    res.send({ status: true, message: `Bag ${bag_number} turned off` });
  } catch (ex) {
    console.error(ex);
    const status = ex.status || 401
    res.status(status).send({ status: false, message: ex.message });
  }
});

router.get('/controllers', async function (req, res, next) {
  let controllers = await listControllers();
  res.send(controllers);
});

router.get('/', async function (req, res, next) {
  try {
    const bags = await getBags()
    res.send(bags)
    return
  } catch (err) {
    console.error(err)
  }
  res.send({})
});

router.post('/assign/:bagNumber/:controller', async function (req, res, next) {
  const bagNumber = req.params.bagNumber;
  const mac = req.params.controller;
  if (bagNumber && mac) {
    const controllers = await listControllers();
    const controllerAssigned = controllers.find(controller => {
      return controller.bag_number === bagNumber && controller.mac === mac
    })
    if (controllerAssigned) {
      await assignBagNumber({ bagNumber: 0, mac: controllerAssigned.mac })
    }
    await assignBagNumber({ bagNumber, mac })
    return res.send({ status: true, message: `Bag ${bag_number} assigned` })
  }
});

router.post('/unassign/:bagNumber/:controller', async function (req, res, next) {
  const bagNumber = req.params.bagNumber;
  const mac = req.params.controller;
  if (bagNumber && mac) {
    await assignBagNumber({ bagNumber: 0, mac })
    return res.send({ status: true, message: `Bag ${bagNumber} unassigned` })
  }
  return res.send({ status: false, message: `Bag number and mac must be specified` })
});

router.get('/set_selected_true/:ip', async function (req, res, next) {
  const ip = req.params.ip;
  await axios.get("http://" + ip + ":667/set_selected_true");
  res.send({ status: true, message: "Selected true" });
})

router.get('/set_selected_false/:ip', async function (req, res, next) {
  const ip = req.params.ip;
  await axios.get("http://" + ip + ":667/set_selected_false");
  res.send({ status: true, message: "Selected false" });
})

module.exports = router;
