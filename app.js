const express = require('express');
const cors = require('cors');
const figlet = require('figlet');
const { client } = require('./data/redis');

console.log('playbrooklyn initializing :', process.env.NODE_ENV);
figlet('BROOKLYN', function (err, data) {
  if (err) {
    console.log('Something went wrong...');
    console.dir(err);
    return;
  }
  console.log(data)
});


console.log('Environment :', process.env.NODE_ENV);
console.log('App Version :', process.env.BACKEND_VERSION);
console.log('Playserver path :', process.env.playserver_path);

const indexRouter = require('./routes/index');
const centersRouter = require('./routes/centers');
const fitzoneRouter = require('./routes/fitzone_dummy');
const classRouter = require('./routes/classes');
const roundsRouter = require('./routes/rounds');
const bagsRouter = require('./routes/bags')

const bagsDummy = require('./routes/bags_dummy')
const dummyClassRouter = require('./routes/dummy_class')
const fitboxersRouter = require('./routes/fitboxers');
const tournamentRouter = require('./routes/tournaments');
const musicRouter = require('./routes/music');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use('/public/',express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/centers', centersRouter);
app.use('/fitboxers', fitboxersRouter);
app.use('/fitzone', fitzoneRouter);
app.use('/class', classRouter);
app.use('/rounds', roundsRouter);
app.use('/dummy_class', dummyClassRouter);
app.use('/bags', bagsRouter);
app.use('/bags_dummy', bagsDummy);
app.use('/tournament', tournamentRouter);
app.use('/music', musicRouter);


app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  console.error(err.response ? err.response.data : err);
  res.send({ 'status': false, 'message': err.message })
});

/**
 * Suscribe to playbrooklyn redis channel
 * 1. Listen for messages
 * 2. Send Heartbeat info on interval of time basis
 *   - info could be: 
 *      HD Usage, 
 *      Memory consumption
 */

app.listen(async () => {
  const bootstrap = require('./services/bootstrap')
  await client.connect();
  bootstrap.init();
  console.log(`boostraped`)
})

module.exports = app;
