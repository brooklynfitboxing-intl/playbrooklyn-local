module.exports = {
    "GOLD": {
        "red": 255,
        "green": 80,
        "blue": 0
    },
    "SILVER": {
        "red": 205,
        "green": 120,
        "blue": 35
    },
    "BRONZE": {
        "red": 45,
        "green": 5,
        "blue": 0
    },
    "PURPLE": {
        "red": 160,
        "green": 0,
        "blue": 100
    },
    "CYAN": {
        "red": 0,
        "green": 189,
        "blue": 80
    },
    "RED": {
        "red": 255,
        "green": 0,
        "blue": 0
    },
    "WHITE": {
        "red": 255,
        "green": 255,
        "blue": 255
    },
    "BLUE": {
        "red": 80,
        "green": 180,
        "blue": 60
    },
    "TEAM_A": {
        "red": 255,
        "green": 30,
        "blue": 0
    },
    "TEAM_B": {
        "red": 210,
        "green": 210,
        "blue": 100,
    },
}