#!/usr/bin/python3

#To parse the response
import json
import socket
import time
import os
import os.path
import requests
import threading
import sys
import redis

#To access remote URL
import urllib3

#To get the mac address
from uuid import getnode as get_mac
from Utils import *


# PRE deploy (In production this should be 'localhost'
#playServerHost  = 'http://54.208.83.7:8080'
playServerHost = 'http://localhost:8080'

path = "/home/bags_firmware_zips/"


def update_connection_test_to_redis(bag, is_success):
	print("[review_existing_bags.py::update_connection_test_to_redis] Updating connection test results to Redis")
	r = redis.Redis(host="localhost", port=6379, db=0)
	conn_test_json = r.hget("test:conn_test_by_mac", bag["mac"])
	my_conn_test = None
	if (conn_test_json != None):
		my_conn_test = json.loads(conn_test_json.decode('UTF-8'))

	else:
		my_conn_test = {}
		my_conn_test["number_of_test"] = 0
		my_conn_test["number_of_failed"] = 0

	my_conn_test["number_of_test"] = my_conn_test["number_of_test"] + 1
	print("Number of test updated")
	if (not is_success):
		my_conn_test["number_of_failed"] =  my_conn_test["number_of_failed"] + 1
		my_conn_test["alive"]= False
	else:
		my_conn_test["alive"]= True

	r.hset("test:conn_test_by_mac", bag["mac"], json.dumps(my_conn_test))


def update_bag_firmware(ip_bag, islog):
	http = urllib3.PoolManager(timeout=1.0)
	url = 'http://' + ip_bag + ':666/currentFirmwareVersion'
	try:
		bag_current_version = http.request('GET', url).data.decode('utf-8')
		if islog == True:
			print("[review_existing_bags.py] Current Version of bag firmware is:" + bag_current_version)

		if bag_current_version != latest_file:

			url = 'http://' + ip_bag + ':666/updateFirmware'
			fin = open(path + latest_file, 'rb')
			files = {'file': fin}
			try:
				r = requests.post(url, files=files)
	#			print(r.text)
			finally:
				fin.close()
			if islog == True:
				print("[review_existing_bags.py] Bag firmware updated from version: " + bag_current_version + " to version:" + latest_file)
		else:
			if islog == True:
				print("[review_existing_bags.py] Bag is already in last version: " + latest_file + ", nothing to do")
	except Exception as ex:
		print("Unable to update software version to bag at ip: "+ip_bag)

def set_bag_as_disconnected(bag, islog):
	send_bag_incidence(6, '[review_existing_bags.py] Bag Controller is disconnected. Bag number ' + str(bag['bag_number']), '', bag['mac'])
	url = playServerHost + '/api/bags/checkForSameIp/' + bag['mac'] + '/DISCONNECTED'

	try:
# Talk to local server to register the location of this MAC to IP "DISCONNECTED"
		if islog == True:
			print("[review_existing_bags.py] Unable to find bag at ip:" + bag['ip'] + " with MAC: " + bag['mac'] + " and bag number: " + str(bag['bag_number']) + " so set status to DISCONNECTED")
		http3 = urllib3.PoolManager(timeout=1.0)
		playServerResponse = http3.request('GET', url).data.decode('utf-8')
#		print("\nThis is the server response when setting bag as DISCONNECTED on url:" + url + "\n " + playServerResponse)
	except Exception  as ex:
		if islog == True:
			print("[review_existing_bags.py] Error trying to connect to local api checkForSomeIp: " + url)
		send_incidence(9, "[review_existing_bags.py] Error trying to set bag as disconnected", "Review existing bags was trying to set bag as disconnected");

def delete_bag(bag, islog):
#	send_incidence(6, "Bag Controller is disconnected. Bag number " + bag['bag_number'] + "")
	url = playServerHost + '/api/bags/' + bag['mac'] + '/remove'
	try:
		http = urllib3.PoolManager(timeout=1.0)
		if islog == True:
			print("\n[review_existing_bags.py] Bag is unassigned so I will delete from database on url:" + url)
		response = http.request('POST', url).data.decode('utf-8')
	except Exception as ex:
		print ("[review_existing_bags.py] Error deleting bag. Url:" + url + "\n")
		send_incidence(9, "[review_existing_bags.py] Error connecting to local API at endpoint: " + url, "Review existing bags was trying to delete bag")

print("[review_existing_bags.py] Executing review of bags in database. For log info run: review_existing_bags.py log")

first_argument = ""
islog = False

try:
	first_argument=sys.argv[1]
except Exception as ex:
	first_argument = ""

if(first_argument == "log"):
	islog = True
	print("[review_existing_bags.py] Review existing bags with log activated\n")

latest_file= ""
os.chdir(path)
list_of_files = os.listdir(path)
list_of_files = sorted(list_of_files, key=os.path.getmtime)
if not list_of_files:
	if islog == True:
		print("[review_existing_bags.py] Firmware not found\n")
	send_incidence(5, "[review_existing_bags.py] Bags firmware not found in minipc at " + path, "Probably problems updating bags firmware")
else:
	latest_file = list_of_files[0]
	if islog == True:
		print("\n[review_existing_bags.py] Last version of bags firmware is:" + latest_file)

#Get number of bags from center configuration
number_of_bags = get_center_number_of_bags()
if (number_of_bags == None):
	print("[review_existing_bags.py] There is not number of bags in database!, exiting review_existing_bags ")
	exit()


#Get bags from database

bags = []
try:
	http = urllib3.PoolManager(timeout=1.0)
	url = playServerHost+'/api/bags'
	if islog == True:
		print("[review_existing_bags.py] Trying to get bags from url: "+url)
	playServerResponse = http.request('GET', url).data.decode('utf-8')
	if islog == True:
		print("\n[review_existing_bags.py] This is the server response: "+playServerResponse)
	playServerRespJson = json.loads(playServerResponse)
	bags = playServerRespJson['data']

except Exception as ex:
		send_incidence(9, "[review_existing_bags.py] Error connecting to local API at endpoint: " + url, "")

		print("[review_existing_bags.py]  Error trying to get bags from local server")


#If any bag number doesn't exist create an entry in database


for i in range(1, number_of_bags+1):
	found = False
	for bag in bags:
		if bag['bag_number'] == i:
			found = True
	if found == False:
		url = playServerHost+'/api/bags/createEmptyBag/'+str(i)
		try:
			http = urllib3.PoolManager(timeout=1.0)
			playServerResponse = http.request('GET', url).data.decode('utf-8')

		except Exception as ex:
			send_incidence(9, "[review_existing_bags.py] Error connecting to local API at endpoint: " + url, "Review existing bags was trying to set bag as disconnected")

			if islog == True:
				print("[review_existing_bags.py] Error setting bag as disconnected")
				print(ex)



# - Pregunto a la base de datos los sacos que habia conectados en la ultima vuelta
# - ¿Tengo Direccion IP?
#	- DISCONNECTED: No hacer nada. Cuando se reconecte, otro proceso del bag_scanner_deamon se encargará de actualizar su IP
#	- SI TENGO IP:
#		 - Trato de conectar con el saco en la direccion ip que habia en base de datos
#       		   CONECTA: Si hay conexion verifico su versión del software preguntando al saco y actualizo si es el caso
#      			   NO_CONECTA: Si no hay conexion miro si a este saco habia un numero de saco asignado
#				NO_CONECTA y SI_ASIGNADO: Si hay un numero de saco asignado actualizo el status en la base de datos a "DISCONNECTED"
#          			NO_CONECTA y NO_ASIGNADO: Si no hay un número de saco asignado lo borro de la base de datos


threads = []


for bag in bags:
	bag_mac = ""
	host = bag['ip']
	url = ""
#	if islog==True:
#		print("Host: "+ host)
	try:
		if (host != "DISCONNECTED") and (host != ""): 
			http = urllib3.PoolManager(timeout=0.6)
			url = 'http://' + host + ':666/whoAreYou'
#			print("Trying to connect to existing bag on this url: "+url)
			bag_mac_response = http.request('GET', url)
			data = bag_mac_response.data.decode('utf-8')
			#print("Data Received from bag number: "+ str(bag['bag_number']) + "is: " + data)
			data_json = json.loads(data)

			bag_mac = data_json['mac']
			bag_number = data_json['bag_number']

			update_connection_test_to_redis(bag, True)

#			print ("\n [Review_Existing_Bags] Found Bag at ip: " + host + " with MAC: " + bag_mac + " and bag_number:" + str(bag_number))
			if latest_file != "":

				t = threading.Thread(target=update_bag_firmware, args=(host, islog, ))
				threads.append(t)
				t.start()
		#	print("No ip for bag")

		# If the bag is DISCONNECTED and there is not bag_number assigned, delete the bag
#			if (str(bag['bag_number']) == "0"):
#				delete_bag(bag)

	except Exception as e:
		print(e)
		print("[Review_existing_bags] Unable to connect to bag number " + str(bag["bag_number"]) + " at ip:" + host)
		# If I am here is because the controller is not connected anymore.
		# If in the database this is not assigned to any bag, then delete from database
		# If it is assigned we will not delete it, but will update the IP with DISCONNECTED"

#		myurl = playServerHost+'/api/bags/checkForSameIp/' + bag['mac'] + '/'+host
#		playServerResponse = http.request('GET', myurl).data.decode('utf-8')
#		print("\nThis is the server response when updating bag: url: " + myurl + "response:" + playServerResponse)
#		playServerRespJson = json.loads(playServerResponse)
#		new_bag_number = playServerRespJson['data']['bag_number']
		#print("\nThis is the new bag number:" + new_bag_number)

		if bag['bag_number'] == 0:
			print("This controller is not connected and unassigned so I delete it from database")
			delete_bag(bag, islog)
		else:
			print("CONTROLLER UNREACHABLE! This controller assigned to bag number " + str(bag['bag_number'])  + " is not connected, updating status in database")
			set_bag_as_disconnected(bag, islog)
			update_connection_test_to_redis(bag, False)
			send_incidence(6, "[review_existing_bags.py] Bag controller number " + str(bag["bag_number"]) + " is unreachable", "url: "+url , 'Mac: '+str(bag["mac"]))


	if(host == "DISCONNECTED" and bag['bag_number']==0):
		print("[Review_existing_bags] Deleting from database entry with mac "+bag["mac"])
		delete_bag(bag, islog)

