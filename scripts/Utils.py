
	# This file contains methods to create incidences and post them to mini-pc

#!/usr/bin/python3
import sys
import urllib3
import os
import json
from uuid import getnode as get_mac
import requests

def send_incidence(incidence_type_id, name, description):
	mac = str(get_mac())
	send_bag_incidence(incidence_type_id, name, description, mac)


def send_bag_incidence(incidence_type_id, name, description, mac):

	print("[Utils.py::send_incidence] sending incidence. Type_id:  " + str(incidence_type_id) + ", name:" + str(name) + ", description:" + str(description))


	center_remote_id = get_center_remote_id()

	if (center_remote_id == None):
		center_remote_id = 0

	incidence = '[{"name":"' + name + '", "center_id":' + str(center_remote_id) + ' ,"description":"' + description + '", "incidence_type_id":"' + str(incidence_type_id) + '", "source_mac":"' + mac +'"}]'

	endpoint = ''

	incidences = []
	incidences.append(incidence)

	endpoint = "http://localhost:8080/api/incidences"
	try:
		http = urllib3.PoolManager(timeout=3.0)

		response = http.request('POST', endpoint, body=incidence)

		response = http.request('POST', endpoint, headers={'Content-Type': 'application/json'}, body=incidence)
		http_status = response.status
#		headers = {}
#		headers= {'Content-Type': 'application/json', 'Accept':'application/json'}
#		r = requests.post(endpoint, headers=headers, data=incidences)
#		r = requests.post(endpoint, data=json.dumps(incidences))

#		http_status = r.status_code
		print("[Utils.py::send] Incidence sent to " + endpoint+ " status:" + str(http_status))
		#print(response.data.decode('utf-8'))
		if(http_status == 201):
			print("[Utils.py::send] Incidence sent correctly")
			return True
		else:
			print("[Utils.py::send_incidence] Error sending incidence")
			return False
	except Exception as e:

		print("[Utils.py::send_incidence] Unable to send Incidence to " + endpoint)
		print(e)
		return False


def internet_on():
	try:
		http = urllib3.PoolManager(timeout=3.0)
		response = http.request('GET', 'http://google.com', headers={'Content-Type': 'application/json'})
		print("Internet connexion is good!")
		return True
	except:
		return False

def get_env_value(key):
	env_file_route = os.path.dirname(os.path.abspath(__file__)) + "/../.env"
	env_file = open(env_file_route, "r")
	lines = env_file.readlines()
	for line in lines:
		splitted = line.split("=")
		try:
			mykey = splitted[0]
			value = splitted[1]
			value = value.replace("\n", "")
			if (mykey == key):
				return(value)
		except:
			i = 0  #avoid errors
	print("Unable to find env variable " + key + " at file: " + env_file_route)

	return(False)


def get_center_remote_id():

	center = get_center()
	try:
		remote_id = center["remote_id"]
		return remote_id
	except Exception as e:
		print("[Utils.py::get_center_remote_id()] Unable to get center remote info from local server " + endpoint)
		return None

def get_center_number_of_bags():

	center = get_center()

	try:

		number_of_bags = center['numberOfBags']
		if (number_of_bags != None):
			return number_of_bags
		else:
			print("Number of bags is undefined")
			send_incidence(11, "Number of bags is undefined in center configuration", "")
			return None
	except:
		print("[Utils.py::get_center_number_of_bags()] Number of bags is undefined")
#		send_incidence(11, "Number of bags is undefined in center configuration", "")
		return None

def get_center_git_branch():

	center = get_center()

	try:

		git_branch = center['git_branch']
		if (git_branch != None):
			return git_branch
		else:
			print("Git Branch is undefined")
#			send_incidence(11, "Git Branch is undefined in center configuration", "")
			return None
	except:
		print("[Utils.py::get_center_git_branch()] Git Branch bags is undefined")
#		send_incidence(11, "Git Branch is undefined in center configuration", "")
		return None




def get_center():
	url = 'http://localhost:8080/api/centers/2'
	try:
		http = urllib3.PoolManager(timeout=1.0)
		playServerResponse = http.request('GET', url).data.decode('utf-8')
		playServerRespJson = json.loads(playServerResponse)
		#centers = playServerRespJson['data']
		try:
		#	center = centers[1]
			center = playServerRespJson
			#print("Center: " + center["name"])
			return center
		except:
			print("No center with id= 2")
			return None
	except Exception as e:
		print("Error: No center defined with id = 2? ")
		print(e)
		return None
