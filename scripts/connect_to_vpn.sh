#!/bin/bash

### BEGIN INIT INFO
# Provides:          Connect to Brooklyn VPN
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: foo service
# Description:       Run VPN service
### END INIT INFO

# Carry out specific functions when asked to by the system


case "$1" in
  start)
    echo "Starting VPN connection..."
    sudo -u root bash -c 'cd /etc/init.d/ && openvpn --config /var/www/playserver/file_vpn/file.ovpn &'
    ;;
  stop)
    echo "Stopping VPN. really is not stopping nothing..."
    pkill openvpn
    sleep 2
    ;;
  *)
    echo "Usage: /etc/init.d/connect_to_vpn.sh {start|stop}"
    exit 1
    ;;
esac

exit 0
