import sys
import urllib3
import time

filename = sys.argv[1]
print (filename)

my_clock = 1000 * time.time()

#video_info = '{"filename":"03. BR116 ROUND01.mp4"}'
video_info = '{"filename":"' + filename + '", "start_clock":"'+ str(my_clock)  +'"}'

endpoint = "http://192.168.20.125:8080/api/start_video?filename="+filename + "&my_clock="+str(my_clock)

http = urllib3.PoolManager(timeout=10.0)

#response = http.request('POST', endpoint, headers={'Content-Type': 'application/json'}, body=video_info)
response = http.request('GET', endpoint)

print(response.data.decode('utf-8'))
