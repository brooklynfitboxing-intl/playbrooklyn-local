PB_FOLDER=/var/www/playserver/
PB_START_SERVICE=start_all_deamons_service.sh
UPDATE_SERVICE=playbrooklyn_update_service

FILE_UPDATE_SCRIPT=/home/update_playbrooklyn.py
NGINX_INSTALLED=/home/nginx_installed

if [ ! -f "FILE_UPDATE_SCRIPT" ]; then
    pip3 install python-dotenv
fi

if [ ! -f "NGINX_INSTALLED" ]; then
	/var/www/playserver/scripts/install_nginx.sh & 
	touch $NGINX_INSTALLED
fi


cp -R -u -p /var/www/playserver/scripts/update_playbrooklyn.py /home/update_playbrooklyn.py
chmod +x /home/update_playbrooklyn.py

cmp --silent $PB_FOLDER/scripts/$UPDATE_SERVICE  /etc/init.d/$UPDATE_SERVICE  || { cp -u -p $PB_FOLDER/scripts/$UPDATE_SERVICE  /etc/init.d/$UPDATE_SERVICE; update-rc.d $UPDATE_SERVICE defaults; }

cmp --silent $PB_FOLDER/scripts/$PB_START_SERVICE  /etc/init.d/$PB_START_SERVICE  || { cp -u -p $PB_FOLDER/scripts/$PB_START_SERVICE  /etc/init.d/$PB_START_SERVICE; update-rc.d $PB_START_SERVICE defaults; }

cmp --silent $PB_FOLDER/scripts/file_conf/crontab  /etc/crontab  ||  cp -u -p $PB_FOLDER/scripts/file_conf/crontab  /etc/crontab 

cmp --silent $PB_FOLDER/scripts/file_conf/mysqld.cnf  /etc/mysql/mysql.conf.d/mysqld.cnf ||  cp -u -p $PB_FOLDER/scripts/file_conf/mysqld.cnf  /etc/mysql/mysql.conf.d/mysqld.cnf

chmod 600 /etc/crontab
service cron reload


mkdir -p /home/scripts
mkdir -p /home/scripts/executed


echo "Starting Playserver at start Up"


sleep 5
	
#/var/www/playserver/scripts/restore_openvpn_connection_service.sh &

#/var/www/playserver/scripts/deploy.sh master &

echo "Starting Bag Scanner Deamon"
/var/www/playserver/scripts/bag_scanner_deamon.py &

echo "starting syncronizer deamon"
/var/www/playserver/scripts/syncronizer_deamon.py &

#echo "Executing pull and deploy"
#/var/www/playserver/scripts/pull_and_deploy_deamon.py &

#echo "Google chrome restore : \n"

#/var/www/playserver/scripts/restore_chrome.sh &





echo "Starting update_deamon"

cd /home/minipc/

chmod 777 .profile
chmod 777 .xsession*
chmod 777 .Xauthority

cd /var/www/playserver
chmod 777 -R *



cd /var/www/playserver/scripts
/var/www/playserver/scripts/update_bags_firmware.sh &


su - minipc

cd /var/www/playserver
php artisan sync:all &


xrandr -s 1280x1024 
