#!/usr/bin/python3
import os
from os.path import join, dirname
from dotenv import load_dotenv
import requests
import sys
import urllib.request
import logging
from zipfile import ZipFile
import hashlib
import shutil
from distutils.dir_util import copy_tree
dotenv_path = join(dirname('/var/www/playserver/'), '.env')
load_dotenv(dotenv_path)

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',filename='/home/update_pb20.log',level=logging.DEBUG)

logging.debug('--------- Update playbrooklyn 2.0 executed --------- ')

HOST = "http://"+os.getenv('PLAY_SERVER_REMOTE')
PORT = os.getenv('PLAY_SERVER_REMOTE_PORT')
#PORT = '8090'
OUTPUT_FOLDER="/var/www/playserver/"
DOWNLOAD_FOLDER="/tmp"

def download_file(center_remote_id,VERSION):
    logging.debug('Downloading new playbrooklyn version ')
    url  = 'http://localhost:8080/api/centers/2' 
    resp = requests.get(url)
    if resp.status_code == 200:
        center = resp.json()
        center_remote_id = str(center['remote_id'])
        VERSION = center['version']

    else:
        logging.error('No se ha consultado el id, proyecto en localhost no disponible  ')
        
    logging.info('Center remote id: {}   Current version: {} '.format(center_remote_id,VERSION))
    logging.info('Request :  '+HOST+':'+PORT+'/api/check_for_update/{}'.format(center_remote_id))
    resp = requests.get(HOST+':'+PORT+'/api/check_for_update/{}'.format(center_remote_id),{'version': VERSION})
    if resp.status_code != 200:
        logging.error('center id {} '.format(center['remote_id']))
        # This means something went wrong.
        raise Exception('GET : '+HOST+':'+PORT+'/api/check_for_update/{}/{} Response : {}'.format(center_remote_id,VERSION,resp.status_code))
            
    update_response =  resp.json()
    logging.debug('response : {}'.format(update_response))
    #fileResponse  = urllib.request.urlopen(response['url'])
    if update_response['status'] == False :
        file_data = {"status":False}
        return file_data  
    url = update_response['url']
    filename = url[url.rindex('/')+1:]
    logging.debug('File to download : '+url)
    file_path = DOWNLOAD_FOLDER + '/'+filename
    try:
        fileResponse  = urllib.request.urlopen(url)
        with open(file_path,'b+w') as file:
            file.write(fileResponse.read())
    except:
        raise Exception('Error al descargar fichero')
        
    file_data = {"status":True,"file_path":file_path, "sha1":update_response['sha1'],"version": update_response['version']}
    return file_data
        
def checkAndExtractFile(filename,remote_sha1):
    logging.info('Check file SHA1 and download ')
    sha1sum_str = sha1sum(filename)
    if (sha1sum_str == remote_sha1):
        logging.debug('checksum verified correctly ')
        # Create a ZipFile Object and load sample.zip in it
        with ZipFile(filename, 'r') as zipObj:
           logging.debug('Extract file :  '+filename + ' in process')
           # Extract all the contents of zip file in current directory
           zipObj.extractall(DOWNLOAD_FOLDER)
           logging.debug('File extracted successfully')
    else:    
        logging.error(' checksum verification   failed ')
        return "error"

def sha1sum(filename):
    h  = hashlib.sha1()
    b  = bytearray(128*1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda : f.readinto(mv), 0):
            h.update(mv[:n])
    return h.hexdigest()

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)    
            
def updateCenterVersion(VERSION):
    logging.debug('Update new playbrooklyn version ')
    url  = 'http://localhost:8080/api/centers/2/change_version/'+VERSION 
    update_response = requests.post(url.format(VERSION))
    if update_response.status_code != 200:
        logging.error('center id {} ')
        raise Exception("POST {} response:  {}".format(url,update_response.status_code))
    logging.debug('response : {}'.format(update_response))
    
    

center_remote_id=1
VERSION="0"
file_data = download_file(center_remote_id,VERSION)

if(file_data['status'] != False):
    file_path = file_data['file_path']
    file_sha1 = file_data['sha1']
    new_version = file_data['version']
    print(file_path)
    print(file_sha1)
    checkAndExtractFile(file_path,file_sha1)
    shutil.rmtree(DOWNLOAD_FOLDER+'/playserver/.git', ignore_errors=True)
    #os.system('rm '+OUTPUT_FOLDER+'/* -rf')
    os.system("rsync -aq --delete --exclude='storage/' "+DOWNLOAD_FOLDER+"/playserver/  "+OUTPUT_FOLDER )
    #copytree(DOWNLOAD_FOLDER+"/playserver/",OUTPUT_FOLDER,symlinks=True)
    #os.system('rm '+OUTPUT_FOLDER+'/public/storage -rf')
    os.system('mkdir -m777 -p '+OUTPUT_FOLDER+'/storage/framework/cache')
    os.system('mkdir -m777 -p '+OUTPUT_FOLDER+'/storage/framework/sessions')
    os.system('mkdir -m777 -p '+OUTPUT_FOLDER+'/storage/framework/views')
    os.system('chmod 777 '+OUTPUT_FOLDER+'  -R')
    os.system('chmod 777 '+OUTPUT_FOLDER+'bootstrap/cache  -R')
    os.system('chmod 777 '+OUTPUT_FOLDER+'storage/*  -R')
    os.system('php '+OUTPUT_FOLDER+'artisan storage:link')
    os.system('php '+OUTPUT_FOLDER+'artisan migrate')  
    updateCenterVersion(new_version)
else:
    logging.debug('Center already updated ')