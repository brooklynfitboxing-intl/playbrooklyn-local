#!/bin/bash

BACKEND_ZIP=pb-backend.zip
FRONTEND_ZIP=pb-frontend.zip

echo "camaleon!" | sudo -S rm /tmp/pb* -rf
echo "camaleon!" | sudo -S rm /tmp/spa -rf
mkdir /tmp/pb-front
mkdir /tmp/pb-local

echo "camaleon!" | sudo -S chown minipc:minipc /var/www/playbrooklyn -R

echo "camaleon!" | sudo -S rm /tmp/PB*.zip

cd /tmp/ && wget http://pro.play.brooklynfitboxing.com:81/pb/$BACKEND_ZIP
cd /tmp/ && wget http://pro.play.brooklynfitboxing.com:81/pb/$FRONTEND_ZIP

unzip -o -qq /tmp/$FRONTEND_ZIP -d /tmp/pb-front/
unzip -o -qq /tmp/$BACKEND_ZIP -d /tmp/pb-local/

rsync -avz -q /tmp/pb-front/ /var/www/playbrooklyn/ --delete
rsync -avz -q /tmp/pb-local/  /home/minipc/playbrooklyn-server/ --delete
echo "camaleon!" | chown minipc:minipc /home/minipc/playbrooklyn-server -R
echo "Actualizacion finalizada"

echo "camaleon!" | sudo -S chown minipc:minipc /home/minipc/.pm2 -R