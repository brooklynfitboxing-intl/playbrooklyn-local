#!/usr/bin/python3

import os


os.system("/var/www/playserver/scripts/get_ips_of_connected_bags.py")

filepath = '/home/bag_ips.txt'

with open(filepath) as fp:
	line = fp.readline()
	while line:
		comm = "/var/www/playserver/scripts/clone_at_ip.sh "+line.strip()
		print("Executing: "+comm)
		try:
			os.system(comm)
			line = fp.readline()
		except Exception as ex:
			print("Error trying to clone:")
			print(ex)


