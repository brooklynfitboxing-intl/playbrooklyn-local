#!/bin/bash

### BEGIN INIT INFO
# Provides:          start_all_deamons_at_start
# Required-Start:    $local_fs $network apache2 mysql playbrooklyn_update_service
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Runs playserver scripts at start
# Description:       Runs playserver scripts at start
### END INIT INFO

# Carry out specific functions when asked to by the system

case "$1" in
  start)
    echo "Starting all Brooklyn startup services..."
    bash -c '/var/www/playserver/scripts/start_all_deamons_at_start.sh &'
    ;;
  stop)
    echo "Stopping playserver daemons."
    pkill start_all_deamons_at_start
    pkill watcher.sh
    pkill php
    pkill python3
    sleep 3
    ;;
  *)
    echo "Usage: /etc/init.d/start_all_deamons_at_start.sh {start|stop}"
    exit 1
    ;;
esac

exit 0

