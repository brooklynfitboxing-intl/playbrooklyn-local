#!/bin/sh


rm  /root/.ssh/id*
rm  /home/minipc/.ssh/id*
cp /var/www/playserver/claves_ssh/*  /root/.ssh/
cp /var/www/playserver/claves_ssh/* /home/minipc/.ssh/


cp /var/www/playserver/claves_ssh/* ~/.ssh

chmod 600  /root/.ssh/*
chmod 600  /home/minipc/.ssh/*


cd /home
echo "Remove /home/bag if exists"
rm -Rf /home/bag

#sudo git clone https://brooklynfitboxing@bitbucket.org/brooklynBoxing/bag.git
echo "Clonning repo..."
git clone git@bitbucket.org:brooklynBoxing/bag.git

#Comprimir archivo
cd /home/bag


#Todo: Tomar el branch de la config del centro
git checkout dev


cd /home/bag
#sudo rm /home/last_commit.txt
#git log --pretty=format:'%h' -n 1  >> /home/last_commit.txt

COMMIT_VERSION="$(git log --pretty=format:'%h' -n 1)"

echo "Commit version:"
echo "${COMMIT_VERSION}"

#Todo: crear el nombre con la ultima versión del commit y con el branch correcto


/bin/rm -f /home/bags_firmware_zips/*

tar -zcvf /home/bags_firmware_zips/bag_firmware_dev_$COMMIT_VERSION.tar.gz /home/bag

echo "Zip file created"

if [$COMMIT_VERSION = ""]
then
	echo "Problem. Not version found"
else
	/bin/rm -f /home/bags_firmware_zips/*

	tar -zcvf /home/bags_firmware_zips/bag_firmware_dev_$COMMIT_VERSION.tar.gz /home/bag

	echo "Zip file created"
fi
