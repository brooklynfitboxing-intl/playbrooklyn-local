#!/usr/bin/python3
#To parse the response
import json
import socket
import time
import os
import redis

#To access remote URL
import urllib3

from Utils import *

# PRE deploy (In production this should be 'localhost'
#playServerHost  = 'http://54.208.83.7:8080'
playServerHost = 'http://localhost:8080'


def set_time_to_bag(my_ip, host, bag_number, mac):
# Aqui se configura cuanto retraso (en milisegundos) acepto en mensaje de ida y vuelta sin considerar un error
	response_maximum_time = 5

	average_response_time = 0
	total_sum = 0
	try:
		trials = 0
		for i in range(10):
			trials = trials + 1
			my_clock = int(time.time()*1000.0)
			sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			sock.settimeout(3)
			UDP_PORT = 10000
			MESSAGE = str(my_ip) + ":" + str(my_clock)

	#		print ("UDP target IP:", host)
	#		print ("UDP target port:", UDP_PORT)
	#		print ("message:", MESSAGE)

			my_clock = int(time.time()*1000.0)
			sock.sendto(MESSAGE.encode('utf-8'), (host, UDP_PORT))
			data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
			my_new_clock = int(time.time()*1000.0)
			response_time = my_new_clock - my_clock
			total_sum = total_sum + response_time
			average_response_time = total_sum / trials

			sock.close()
			if (response_time < response_maximum_time):
				break
			else:
				print("Connection to controller takes too long: " + str(response_time) + " milliseconds")
		if(trials >= 10):
			send_incidence(5, "ERROR: Slow network. Average time:" + str(average_response_time), "[SyncronizerDeamon::Set time to bag] Error syncronizing clocks with bag number " + str(bag_number) +  " in IP:" + host + " and MAC:" + mac + ". Response time is exceded after 10 trials. Check PC performance or network connectivity (router) ")
		else:
			if (trials > 1):
				send_incidence(5, "Warning: Slow clock syncronization", "Slow performance in syncronizing clocks with bag number " + str(bag_number) +  ". Check network connectivity (router or cables) ")

		print ("Updated Bag number " + str(bag_number) + " Clock with UDP. Response time=" + str(response_time))
#		print ("received message:"+ str(data))
		return True
	except Exception as ex:
		print("ERROR syncronizing clocks with host: " + host )
		print(ex)
		sock.close()
		return False


while True:

	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.connect(("8.8.8.8", 80))
		my_ip_address = s.getsockname()[0]
		s.close()

		http = urllib3.PoolManager(timeout=1)
		url = playServerHost+'/api/bags'
		print("Getting all bags from mini-pc: " + url)
		playServerResponse = http.request('GET', url).data.decode('utf-8')
#		print("\nThis is the server response: "+playServerResponse)
		playServerRespJson = json.loads(playServerResponse)
		bags = playServerRespJson['data']
		bags_string = json.dumps(bags)

		redisClient = redis.StrictRedis(host='localhost', port=6379, db=0)
		for bag in bags:
			host = bag["ip"]
			mac = bag["mac"]
			bag_number = bag["bag_number"]
			if (host != "" and host != "DISCONNECTED"):
				if(mac != ""):
					print("I will try to synronize IP:" + host + " and mac:" + mac)
					success = False
					try:
						success = set_time_to_bag(my_ip_address, host, bag_number, mac)
					except Exception as ex:
						print("[SyncronizerDeamon.py] Error setting time to IP: "+host)
						print(ex)
#					try:
#						if success:
#							redisClient.hset("bag_sync_status", bag_number, "OK")
#						else:
#							redisClient.hset("bag_sync_status", bag_number, "KO")
#					except Exeption as ex_redis:
#						send_incidence(5, "[Syncronizer deamon] Error connecting to redis in minipc", "Error connecting to redis. Probably redis server is down")
					time.sleep(1)
		time.sleep(30)  # 8 horas=28800 sec
	except Exception as ex:
		print("Error Trying to get bags from local server: ")
		print(ex)
		time.sleep(30)

