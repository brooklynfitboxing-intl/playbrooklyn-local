#!/usr/bin/python3
import time
import sys

#Este  script se ejecuta desde laravel para decirle al bag_scanner_deamon cuando estamos en clase
# pasar como parametro TRUE o FALSE (TRUE es que estamos en clase ahora)

if (len(sys.argv) < 2):
	print("You have to put TRUE or FALSE as an argument")
	exit()


in_class = sys.argv[1]


if (in_class == "TRUE"):
	print("Setting that we are in class as TRUE")
	file = open('/var/www/playserver/we_are_in_class_timeflag.txt', 'w+')
	now_time = int(time.time() * 1000)
	file.write(str(now_time))
	file.close()

if (in_class == "FALSE"):
	print("Setting that we are in class as FALSE")
	file = open('/var/www/playserver/we_are_in_class_timeflag.txt', 'w+')
	#now_time = int(time.time() * 1000)
	file.write("0")
	file.close()



