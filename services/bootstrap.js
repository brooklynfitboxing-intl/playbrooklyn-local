const { get } = require('../data/redis')
const axios = require('axios')
const { networkInterfaces } = require('os');
const nodeCron = require("node-cron");
const { getCenter } = require('./center');

async function reportVpnAddress() {
    try {
        const nets = networkInterfaces();
        const ipAddresses = Object.create(null); // Or just '{}', an empty object
        for (const name of Object.keys(nets)) {
            for (const net of nets[name]) {
                // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
                if (net.family === 'IPv4' && !net.internal) {
                    if (!ipAddresses[name]) {
                        ipAddresses[name] = [];
                    }
                    ipAddresses[name].push(net.address);
                }
            }
        }
        const currentVersion = await get('current_version')
        if (!ipAddresses['tun0']) {
            throw new Error('Minipc not conected to VPN')
        }
        const ip = ipAddresses['tun0'][0];
        const center = await getCenter()
        await axios.default.post(`${process.env.fitzone_url}/info/${center.fitzone_alias}`, {
            version: currentVersion,
            ip,
        })
    } catch (err) {
        console.log(`error reporting vpn address`);
    }
}

function startScheduledJobs() {
    // at every 7th min   
    nodeCron.schedule("*/7 * * * *", reportVpnAddress).start();
}

async function getCenterInformation() {
}

async function init() {

    getCenterInformation()
    // startScheduledJobs()
}


module.exports = {
    init
}