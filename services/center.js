const mysql = require('../data/config');
const util = require('util');
const { set, get } = require('../data/redis');
const query = util.promisify(mysql.query).bind(mysql);
// const centerFile = path.join(`${__dirname}/../public/resources/center.json`);
/**
 * 
 * @returns {Promise<{name:string, numberOfBags:number, fitzone_alias:string, git_branch:string, version:string, environment:string }>}
 */
const getCenter = async () => {
    const center = await get('center');
    if (center) {
        return center
    } else {
        const centers = await query(`SELECT * from centers where id = 2 `)
        if (centers.length === 0) {
            throw new Error('No center defined for this minipc')
        }
        setCenter(centers[0])
        return centers[0];
    }
}
/**
 * 
 * @returns {}
 */
const setCenter = async (centerData) => {
    const centerDataStr = JSON.stringify(centerData, null, 2)
    await set('center', centerDataStr, 3600 * 2)
}

module.exports = {
    getCenter,
    setCenter
}