const subscriber = require('../data/suscriber').default
const { hgetall, hget } = require('../data/redis');
const BAGS_MESSAGES_TYPES = ['HEART_BEAT', 'CONNECT'];
const axios = require('axios');
/**
 * @param { string } ipController
 * @param {{red:number, green:number, blue:number}} colorConfig Color config RGB
 */
const setLightColor = async (ipController, { red, green, blue }) => {
    return axios.post("http://" + ipController + ":667/set_color", { red, green, blue });
}

/**
* @param { string | Array<{ip:string, mac:string, bag_number: string }>} ipController
*/
const turnLightOff = async (ipController) => {
    if (Array.isArray(ipController)) {
        /**
         * @type {Array<Promise<import('axios').AxiosResponse>>} bagsPromises
         */
        const bagsPromises = ipController.map(controller => {
            return new Promise(async (res, rej) => {
                try {
                    const response = await axios.default.get("http://" + controller.ip + ":667/turn_off");
                    res({ ...controller, response: response.data })
                } catch (err) {
                    rej({ ...controller, error: err.message })
                }
            })
        })

        const responses = await Promise.allSettled(bagsPromises);
        const failed = responses.filter(it => it.status === 'rejected')
        if (failed.length) {
            failed.forEach(it => {
                console.log(`Failed to send request to bag $}`)
            })
        }
    } else {
        return axios.get("http://" + ipController + ":667/turn_off");
    }
};
/**
* @param { string | Array<String> } ipController
*/
const turnLightOn = async (ipController) => {
    if (Array.isArray(ipController)) {
        /**
         * @type {Array<Promise<import('axios').AxiosResponse>>} bagsPromises
         */
        const bagsPromises = ipController.map(controller => {
            return new Promise(async (res, rej) => {
                try {
                    const response = await axios.default.get("http://" + controller.ip + ":667/turn_on");
                    res({ ...controller, response: response.data })
                } catch (err) {
                    rej({ ...controller, error: err.message })
                }
            })
        })

        const responses = await Promise.allSettled(bagsPromises);
        const failed = responses.filter(it => it.status === 'rejected')
        if (failed.length) {
            failed.forEach(it => {
                console.log(`Failed to send request to bag $}`)
            })
        }
    } else {
        return axios.get("http://" + ipController + ":667/turn_on");
    }
};

/**
 * A map where keys are the mac address and values ip and bag_number
 * @typedef { { ip:string, bag_number: number, mac:string} } Controller
 * @returns {Promise<Array<Controller>>}
 */
const listControllers = async () => {
    const list = []
    let controllers = await hgetall('controllers') || {}
    for (const mac in controllers) {
        list.push({ mac, ...JSON.parse(controllers[it]) })
    }
    return list;
};

const assignBagNumber = async ({ bagNumber, mac }) => {
    try {
        const controller = await hget('controllers', mac);
        if (controller) {
            const data = JSON.parse(controller)
            await hset('controller', mac, JSON.parse({ ...data, bag_number: bagNumber }))
            await axios.get(`http://${data.ip}:667/setBagNumber?bag_number=${bagNumber}`)
        } else {
            console.warn(`No controller found`)
            return false
        }
        return true;
    } catch (err) {
        console.error(err)
    }
};

/**
* @param { string } ipController
*/
const stopReadingHits = async (ipController) => {
    return axios.get(`http://${ipController}:667/stopVideo`);
};

const rebootController = async ({ delay = 1 , mac }) => {
    let controllers  = [] 
    if (mac) {
        const controller = await hget('controllers', mac);
        controllers = [JSON.parse(controller || '{}' )]
    } else {
        controllers = JSON.parse(await hgetall('controllers') || '[]')
    }
    controllers.forEach( async (controller) => {
        await axios.default.get(`http://${controller.ip}:666/rebootInSeconds?seconds=${delay}`)
    })
    return true;
};

const listenMessages = async () => {
    await subscriber.connect();
    await subscriber.subscribe('bags_conroller', async (msg) => {
        const message = JSON.parse(msg)
        if (BAGS_MESSAGES_TYPES.includes(message.event_type)) {
            await hset(`controllers`, message.mac, JSON.stringify({ from: message.from, connected_on: message.connected_on, last_message: message.timestamp, event_type: message.event_type }))
        }
    })
};

module.exports = {
    setLightColor,
    turnLightOff,
    turnLightOn,
    listControllers,
    listenMessages,
    assignBagNumber,
    stopReadingHits,
    rebootController
}