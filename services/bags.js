const ColorsConfig = require('../public/resources/colors_config')
const mysql = require('../data/config');
const util = require('util');
const axios = require('axios');
const query = util.promisify(mysql.query).bind(mysql);
const { get, hgetall } = require('../data/redis');
const { setLightColor, turnLightOff, turnLightOn } = require('./controllers');

/**
 * 
 * @returns {Promise<Array<{ip:string, bag_number:number, mac:string}>>}
 */
const getBags = async () => {
    const bags = await query(`SELECT ip, bag_number, mac from bags where ip != '' and bag_number!=0  and ip!= 'DISCONNECTED';`)
    return bags;
}
/**
 * 
 * @param {number} bagNumber 
 * @param { 'GOLD' | 'SILVER' | 'BRONZE' | 'PURPLE' | 'CYAN' | 'RED' }  color 
 * @returns 
 */
const setBagColor = async (bagNumber, color) => {
    console.log('Setting bag color', bagNumber, color)
    if (!ColorsConfig[color]) {
        throw { message: "Color " + color + " is not defined yet in colors_config.json" }
    }
    const { red, green, blue } = ColorsConfig[color];
    let bags = [];
    if(bagNumber === 'ALL'){
        bags = await getBags();
    } else {
        bags = await query("SELECT ip, bag_number from bags where ip != '' and  bag_number = ? and ip!= 'DISCONNECTED' ", [bagNumber]);
    }
    if (bags.length == 0) {
        throw { message: "Bag: " + bagNumber + " not found or its not assigned" }
    }
    bags.forEach( async (bag) => {
        setLightColor(bag.ip, { red, green, blue })
    })
    return true
}
/**
 * Receives the number of the bag to turn it off, if  value 'ALL' is received, all the lights will turned it off
 * @param {string  bagNumber 
 * @returns boolean
 */
const turnOffBag = async (bagNumber) => {
    console.log('Turning off bag ', bagNumber)
    let bags = [];
    try { 
        if(bagNumber === 'ALL'){
            bags = await getBags();
        } else {
            bags = await query("SELECT ip, bag_number from bags where ip != '' and   bag_number = ?", [bagNumber]);
        }
        if (bags.length == 0) {
            throw { message: "Bag: " + bagNumber + " not found or its not assigned" };
        }
        await turnLightOff(bags)
    } catch(err){
        console.error(`Error turning off bag ${bagNumber}`,err)
    }
    return true
}
/**
 * Receives the number of the bag to turn it on, if  value 'ALL' is received, all the lights will turned it off
 * @param {string} bagNumber 
 * @returns boolean
 */
const turnOnBag = async (bagNumber) => {
    console.log('Turning ong bag ', bagNumber)
    try {

        let bags = [];
        if(bagNumber === 'ALL'){
            bags = await getBags();
        } else {
            bags = await query("SELECT ip, bag_number from bags where ip != '' and   bag_number = ?", [bagNumber]);
        }
        if (bags.length == 0) {
            throw { message: "Bag: " + bagNumber + " not found or its not assigned" };
        }
        await turnLightOn(bags)
    } catch (err){
        console.error(`Error turning on  bag ${bagNumber}`,err)
    }
    return true
}

const getHitsDetail = async (round_number, bag_number) => {
    const hits = {}
    const hitsRedis = await hgetall(`hits_${round_number}_${bag_number}`);
    for (const key in hitsRedis) {
        hits[key] = JSON.parse(hitsRedis[key])
    }
    return hits
}
/**
 * 
 * @param {number} fitzoneId 
 * @param {number} roundNumber 
 */
const sendHitsDetail = async (fitzoneId, roundNumber) => {
    const bags = await getBags()
    const current_challenge = await get('current_challenge')
    const roundData = await hgetall('round_data');
    const bagHits = {}
    for (const bag of bags) {
        const hitsDetails = await getHitsDetail(roundNumber, bag.bag_number)
        bagHits[bag.bag_number] = hitsDetails
    }
    const result = {
        roundMetadata: JSON.parse(roundData[roundNumber]),
        bagsHits: bagHits
    }
    await axios.post(`${process.env.fitzone_url}/hitsdetails/${fitzoneId}/${current_challenge}/${roundNumber}`, result)
}

const createMoves = ({ start_in, sequence, bpm, number_of_hits }) => {
    const hits = {}
    const beat_delay = 60000 / bpm
    let time_count = start_in
    let sequence_list = sequence.split('')
    sequence_index = 0
    hit_number = 1
    while (hit_number <= number_of_hits) {
        if (sequence_index >= sequence_list.length) {
            sequence_index = 0
        }
        const hit_type = sequence_list[sequence_index]
        if (hit_type != '0') {
            hits[hit_number] = {
                "move": hit_type,
                "perfect_hit_time": time_count,
                "hit_number": hit_number,
            }
            hit_number++;
        }
        time_count = time_count + beat_delay
        sequence_index = sequence_index + 1
    }
    return hits
}
/**
 * 
 * @param {{ip:string, bag_number:string, mac:string}} bag
 * @param {{type_of_video:string, bpm:string, start_in:number, sequence:string, start_clock:string, round_id:number, projector_delay:number, number_of_hits:number, user_power:number}} startVideoMessage 
 * @returns 
 */

async function sendStartVideo(bag, { type_of_video, bpm, start_in, sequence, start_clock, round_id, projector_delay, number_of_hits, user_power }) {
    const startVideoMessage = { type_of_video, bpm, start_in, sequence, start_clock, round_id, projector_delay, number_of_hits, user_power }
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axios.default.post(`http://${bag.ip}:667/startVideo`, startVideoMessage, { timeout: 5000 })
            resolve({ bag: bag.bag_number, ip: bag.ip, data: response.data })
        } catch (err) {
            const errMessage = err.response?.data || err.response?.statusText || err.code
            console.log(errMessage)
            reject({ bag: bag.bag_number, ip: bag.ip, message: errMessage })
        }
    })
}
module.exports = {
    getBags, setBagColor, turnOffBag, turnOnBag, getHitsDetail, createMoves, sendStartVideo, sendHitsDetail
}