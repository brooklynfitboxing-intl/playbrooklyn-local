const VERSION = '22.06.24';
module.exports = {
  apps : [{
    name:'playbrooklyn-server',
    script: './bin/www',
    exec_mode:"fork",
    // Deberia arrancarlo el usuario minipc para que los logs  esten en /home/minipc/.pm2/logs
    watch: false,
    time: true,
    autorestart:true,
    env: {     
      "NODE_ENV": "development",
      "playserver_path": "/var/www/playserver",
      "db_name": "brooklyn",
      "db_user": "root",
      "db_password": "camaleon!",
      "db_host": "10.20.0.119",
      "fitzone_url": "http://10.10.0.4:3005/iot",
      "auth_server": "http://10.10.0.4:3000",
      "server_url": "pro.play.brooklynfitboxing.com",
      "url_server": 'http://pro.play.brooklynfitboxing.com:81/pb/',
      "fwg_server": "http://10.10.0.4:3030",
      'BACKEND_VERSION' : VERSION
    },
    env_production : {
       "NODE_ENV": "production_bf",
       "playserver_path": "/var/www/playserver",
       "db_name": "brooklyn",
       "db_user": "root",
       "db_password": "camaleon!",
       "db_host": "localhost",
       "fitzone_url": "https://myh4c.brooklynfitzone.com/iot/iot",
       "auth_url": "https://brooklynfitzone.com/auth",
       "server_url": "pro.play.brooklynfitboxing.com",
       "url_server": "http://pro.play.brooklynfitboxing.com:81/pb/",
       "fwg_server": "https://brooklynfitzone.com/fwg",
       'BACKEND_VERSION' : VERSION
    },
    env_production_f3 : {
      "NODE_ENV": "production_f3",
      "playserver_path": "/var/www/playserver",
      "db_name": "brooklyn",
      "db_user": "root",
      "db_password": "22604",
      "db_host": "localhost",
      "fitzone_url": "https://app.f3fitbox.com/iot/iot/",
      "server_url": "play.f3fitbox.com",
      "url_server": "http://pro.play.brooklynfitboxing.com:81/f3/"
   }
  }],
};
